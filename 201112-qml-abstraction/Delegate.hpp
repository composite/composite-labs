/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __DELEGATE_HPP__
#define __DELEGATE_HPP__

#include <QtCore/QObject>

class QDeclarativeView;
class Engine;

/*
 * I'm not sure yet what this class will do, yet.  I'm
 * just going to try to get signals and slots to fire
 * with the QML stuff.
 */
class Delegate : public QObject
{
    Q_OBJECT
public:
    Delegate();
    ~Delegate();

    void set_view(QDeclarativeView *view);
    void set_engine(Engine *e);
    void init(); /* call after setting view and e */

private:
    QDeclarativeView *m_view;
    Engine *m_engine;
}; // class Delegate

#endif // __DELEGATE_HPP__
