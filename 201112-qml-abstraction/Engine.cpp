/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Engine.hpp"

#include <QtCore/QDebug>

Engine::Engine() :
    m_mode(M_CLEAR),
    m_display("0.0"),
    m_acc(0.0),
    m_new_mode(true)
{
}

Engine::~Engine()
{
}

QString Engine::get_display()
{
    return m_display;
}

void Engine::keypress(QString val)
{
    qDebug() << "Got keypress " << val;
    if (val.length() != 1) {
        p_error();
        return;
    }

    if (val.at(0).isDigit()) {
        p_input_digit(val);
        return;
    }

    if (val == "=") {
        p_calculate();
        return;
    }

    /* If we're here, it should be an operation
     * if the operation is invalid... it's
     * detected in the method.
     */
    p_input_oper(val);
}

void Engine::p_error()
{
    m_mode = M_CLEAR;
    m_display = "-E-";
    m_acc = 0.0;
    emit content_changed(m_display);
}

void Engine::p_input_digit(QString val)
{
    bool ok;
    double v;
    v = val.toDouble(&ok);
    if (m_new_mode || !ok || (v == 0.0)) {
        m_display = val;
    } else {
        m_display += val;
    }
    m_new_mode = false;
    emit content_changed(m_display);
}

void Engine::p_input_oper(QString val)
{
    char c = val.at(0).toLatin1();
    bool push = true;
    switch (c) {
    case '+':
        m_mode = M_ADD;
        break;
    case '-':
        m_mode = M_SUB;
        break;
    case '*':
        m_mode = M_MUL;
        break;
    case '/':
        m_mode = M_DIV;
        break;
    case 'C':
        push = false;
        p_clear();        
        break;
    default:
        push = false;
        p_error();
    }
    if (push) {
        m_acc = m_display.toDouble();
        m_new_mode = true;
    }
}

void Engine::p_calculate()
{
    double a, b, ans;
    bool error = false;
    bool use_answer = true;

    a = m_acc;
    b = m_display.toDouble();
    switch (m_mode) {
    case M_ADD:
        ans = a + b;
        break;
    case M_SUB:
        ans = a - b;
        break;
    case M_MUL:
        ans = a * b;
        break;
    case M_DIV:
        if (b < 1e-6) {
            error = true;
        } else {
            ans = a / b;
        }
        break;
    case M_CLEAR:
        error = false;
        use_answer = false;
        break;
    default:
        error = true;
        use_answer = false;
    }
    if (error) {
        p_error();
    }

    if (use_answer) {
        m_display = QString::number(ans);
        m_acc = ans;
        m_mode = M_CLEAR;
        m_new_mode = true;
        emit content_changed(m_display);
    }
}

void Engine::p_clear()
{
    m_mode = M_CLEAR;
    m_display = "0.0";
    m_acc = 0.0;
    m_new_mode = true;
    emit content_changed(m_display);
}

