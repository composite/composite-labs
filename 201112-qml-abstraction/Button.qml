/*                                      -*- mode:javascript -*-
 * Button.qml
 *
 * This code was written by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 * in 2011.  It is placed in the public domain.
 */

import QtQuick 1.0

Item {
    /* This should be set by the parent element */
    property string text: "X"

    /* These should EXIST in the parent element, or be set
     * BY the parent element.  They are referred to by the
     * child elements.
     */
    height: parent ? parent.button_height : 42;
    width: parent ? parent.button_width : 42;
    property color button_color_top: parent ? parent.button_color_top : "white";
    property color button_color_bot: parent ? parent.button_color_bot : "gray";
    property color text_color: parent ? parent.text_color : "black";

    /* These properties may be set by the parent element */
    property int font_pixel_size: 24;
    property int radius: (height < width) ? height/8 : width/8;

    /* This signal will fire when we get clicked, and contain the
     * text of the button.
     */
    signal postValue(string val);

    /* Main geometry of button */
    Rectangle {
	id: iRectangle;
	anchors.fill: parent; /* parent is iRoot */
	radius: parent.radius;

	gradient: Gradient {
	    /* parent.button_color_* is implied */
	    GradientStop { position: 0.0; color: button_color_top; }
	    GradientStop { position: 1.0; color: button_color_bot; }
	}
    }

    /* Text display of button */
    Text {
	id: iText;
	text: parent.text; /* without 'parent.' this would be ambiguous */
	color: text_color; /* parent.text_color implied */
	font.pixelSize: font_pixel_size;
	anchors.centerIn: parent;
    }

    /* This enables mouse interaction with button */
    MouseArea {
	id: iMouseArea;
	anchors.fill: parent;
	onClicked: {
	    postValue(text);
	}
    }
}
