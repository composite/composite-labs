/*                                      -*- mode:javascript -*-
 * calculator.qml
 *
 * This code was written by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 * in 2011.  It is placed in the public domain.
 */

import QtQuick 1.0

Rectangle {
    id: iRoot;
    color: "gray";
    width: 200; height: 252;
    anchors.margins: 5;
    property string text: "0";
    focus: true; /* for keyboard input */

    signal data(string val);
    onData: {
	console.log("iRoot: " + val)
    }

    signal contentChanged(string val);
    onContentChanged: {
	text = val;
    }

    signal clear;
    onClear: {
	text = "0";
    }

    signal quit;

    Rectangle {
	id: iDisplay;
	color: "white";
	anchors.top: parent.top;
	anchors.left: parent.left;
	anchors.right: parent.right;
	anchors.margins: parent.anchors.margins;
	height: 42;
	radius: height/10;

	Text {
	    id: iDisplayText;
	    text: iRoot.text;
	    color: "black";
	    font.pixelSize: 24;
	    anchors.verticalCenter: parent.verticalCenter;
	    anchors.right: parent.right;
	    anchors.margins: 10;

	}
    }

    Grid {
	columns: 4;
	property int button_width: (width - 2 * anchors.margins)/columns;
	property int button_height: button_width;
	property color button_color_top: "#8888FF";
	property color button_color_bot: "blue";
	property color text_color: "black";
	spacing: parent.anchors.margins;
	anchors.top: iDisplay.bottom;
	anchors.left: iRoot.left;
	anchors.right: iRoot.right;
	anchors.bottom: iRoot.bottom;
	anchors.margins: parent.anchors.margins;

	/* Row 0 */
	Button { id: i7; text: "7"; }
	Button { id: i8; text: "8"; }
	Button { id: i9; text: "9"; }
	Button { id: iPlus; text: "+"; }

	/* Row 1 */
	Button { id: i4; text: "4"; }
	Button { id: i5; text: "5"; }
	Button { id: i6; text: "6"; }
	Button { id: iMinus; text: "-"; }

	/* Row 2 */
	Button { id: i1; text: "1"; }
	Button { id: i2; text: "2"; }
	Button { id: i3; text: "3"; }
	Button { id: iMultiply; text: "*"; }

	/* Row 3 */
	Button { id: iC; text: "C"; }
	Button { id: i0; text: "0"; }
	Button { id: iEquals; text: "="; }
	Button { id: iDivide; text: "/"; }

    }

    Keys.onPressed: {
	switch (event.key) {
	case Qt.Key_0:
            iRoot.data("0");
            break;
	case Qt.Key_1:
            iRoot.data("1");
            break;
	case Qt.Key_2:
            iRoot.data("2");
            break;
	case Qt.Key_3:
            iRoot.data("3");
            break;
	case Qt.Key_4:
            iRoot.data("4");
            break;
	case Qt.Key_5:
            iRoot.data("5");
            break;
	case Qt.Key_6:
            iRoot.data("6");
            break;
	case Qt.Key_7:
            iRoot.data("7");
            break;
	case Qt.Key_8:
            iRoot.data("8");
            break;
	case Qt.Key_9:
            iRoot.data("9");
            break;
	case Qt.Key_C:
            iRoot.data("C");
            break;
	case Qt.Key_Plus:
            iRoot.data("+");
            break;
	case Qt.Key_Minus:
            iRoot.data("-");
            break;
	case Qt.Key_Asterisk:
            iRoot.data("*");
            break;
	case Qt.Key_Slash:
            iRoot.data("/");
            break;
	case Qt.Key_Equal:
	case Qt.Key_Enter:
	case Qt.Key_Return:
            iRoot.data("=");
            break;
	case Qt.Key_Escape:
            iRoot.quit();
            break;
	}
    }

    /* Component.onCompleted() is more or less a constructor */
    Component.onCompleted: {
	/* N.B. Using 'iRoot.' here is redundant. */
	iC.postValue.connect(iRoot.data);
	iPlus.postValue.connect(iRoot.data);
	iMinus.postValue.connect(iRoot.data);
	iMultiply.postValue.connect(iRoot.data);
	iDivide.postValue.connect(iRoot.data);
	iEquals.postValue.connect(iRoot.data);
	i0.postValue.connect(iRoot.data);
	i1.postValue.connect(iRoot.data);
	i2.postValue.connect(iRoot.data);
	i3.postValue.connect(iRoot.data);
	i4.postValue.connect(iRoot.data);
	i5.postValue.connect(iRoot.data);
	i6.postValue.connect(iRoot.data);
	i7.postValue.connect(iRoot.data);
	i8.postValue.connect(iRoot.data);
	i9.postValue.connect(iRoot.data);
    }
}
