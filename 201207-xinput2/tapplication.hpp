/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __TAPPLICATION_HPP__
#define __TAPPLICATION_HPP__

#include <QApplication>

class TApplication : public QApplication
{
    Q_OBJECT
public:
    TApplication(int argc, char* argv[]);
    virtual ~TApplication();

    virtual bool x11EventFilter(XEvent *event);

    static void sendTouchEventsTo(QWidget *wid);

private:
    void init();
    bool handleX11TouchEvent(XEvent *ev);

private:
    bool m_initialized;
    Display *m_display;
    int m_xi_opcode;
};

#endif // __TAPPLICATION_HPP__
