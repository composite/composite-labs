/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __TOUCHEVENT_HPP__
#define __TOUCHEVENT_HPP__

#include <QEvent>
#include <QPoint>
#include <QRect>

class TouchEvent : public QEvent
{
public:
    enum {
        TouchEventId = QEvent::User + 1
    };

    TouchEvent(int id);
    virtual ~TouchEvent();

    int id() { return m_id; }

private:
    int m_id;
};

#endif // __TOUCHEVENT_HPP__
