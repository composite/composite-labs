/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "tapplication.hpp"

#include <QDebug>
#include <QX11Info>
#include <QWidget>

#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <X11/Xutil.h>

#include <stdexcept>

#include "touchevent.hpp"

TApplication::TApplication(int argc, char* argv[]) :
    QApplication(argc, argv),
    m_initialized(false),
    m_display(0),
    m_xi_opcode(-1)
{
    /* postpone init until first event */
}

TApplication::~TApplication()
{
}

void TApplication::init()
{
    int event, error;

    if (m_initialized)
        return;

    m_initialized = true;

    m_display = QX11Info::display();

    if (!XQueryExtension(m_display, "XInputExtension", &m_xi_opcode,
                         &event, &error))
        throw std::runtime_error("Could not find XInputExtension");
}

/* static */
void TApplication::sendTouchEventsTo(QWidget *w)
{
    XIEventMask mask;
    Window win;
    Display *dpy;

    dpy = QX11Info::display();
    win = w->winId();

    memset(&mask, 0, sizeof(XIEventMask));
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_LASTEVENT);
    mask.mask = (unsigned char*) calloc(mask.mask_len, sizeof(char));

    XISetMask(mask.mask, XI_TouchBegin);
    XISetMask(mask.mask, XI_TouchUpdate);
    XISetMask(mask.mask, XI_TouchEnd);

    XISelectEvents(dpy, win, &mask, 1);

    free(mask.mask);
}

bool TApplication::handleX11TouchEvent(XEvent *ev)
{
    XGenericEventCookie *cookie = &ev->xcookie;
    XIDeviceEvent *de = reinterpret_cast<XIDeviceEvent*>(cookie->data);
    const char *evname = 0;
    QWidget *target;

    switch (cookie->evtype) {
    case XI_TouchBegin: evname = "XI_TouchBegin"; break;
    case XI_TouchUpdate: evname = "XI_TouchUpdate"; break;
    case XI_TouchEnd: evname = "XI_TouchEnd"; break;
    }

    printf("%-15s dev=%02d id=%03d src=%02d eventwin=%08lx "
           "root_x=%7.2f root_y=%7.2f "
           "event_x=%7.2f event_y=%7.2f flags=%08x\n",
           evname, de->deviceid, de->detail, de->sourceid, de->event,
           de->root_x, de->root_y,
           de->event_x, de->event_y, de->flags);

    TouchEvent tev(de->detail);
    target = QWidget::find(de->event);
    return notify(target, &tev);
}

bool TApplication::x11EventFilter(XEvent *ev)
{
    bool handled = false;
    XGenericEventCookie *cookie;

    if (!m_initialized)
        init();

    // All XI2 events are GenericEvents
    if (ev->type != GenericEvent)
        return false;

    cookie = &ev->xcookie;

    if (cookie->extension != m_xi_opcode)
        return false;

    /**********************************************
     * BEYOND THIS POINT ARE ONLY XINPUT EVENTS
     **********************************************/

    /* WORKAROUND: Call XGetEventData() if cookie->data is null
     *
     * Qt on Ubuntu is patched so that XGetEventData()
     * was called on entry, but we're responsible for
     * calling XFreeEventData().
     *
     * However, without the patch then we need to
     * call it ourselves.
     *
     * Strictly speaking, the docs day that
     * cookie->data is "undefined" until you call
     * XGetEventData().  However, in practice it is
     * a null pointer.  Therefore, using this to
     * determine if we need to call XGetEventData().
     */
    if (!cookie->data)
        XGetEventData(cookie->display, cookie);

    switch (cookie->evtype)
    {
    case XI_TouchBegin:
    case XI_TouchUpdate:
    case XI_TouchEnd:
        handled = handleX11TouchEvent(ev);
        break;
    default:
        qDebug() << "XI_somethingelse";
    }

    if (handled)
        XFreeEventData(cookie->display, cookie);

    return handled;
}
