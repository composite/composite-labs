/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "EventRouter.hpp"
#include <QApplication>
#include <QEvent>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QDebug>
#include <iostream>
#include <cassert>

using namespace std;

EventRouter::EventRouter()
{
    setAcceptTouchEvents(true);
    setAcceptedMouseButtons( acceptedMouseButtons() );
}

bool EventRouter::event(QEvent *e)
{
    QDeclarativeItem *parent = parentItem();
    bool handled = false;

    switch(e->type()) {
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove: {
        QMouseEvent *me = dynamic_cast<QMouseEvent*>(e);
        assert(me);
        cout << "MouseEvent" << endl;
        handled = true;
    }   break;
    case QEvent::GraphicsSceneMouseDoubleClick:
    case QEvent::GraphicsSceneMousePress:
    case QEvent::GraphicsSceneMouseRelease:
    case QEvent::GraphicsSceneMouseMove: {
        QGraphicsSceneMouseEvent *me = dynamic_cast<QGraphicsSceneMouseEvent*>(e);
        assert(me);
        QPointF pos = me->scenePos();
        QList<QObject*> children = parent->children();
        QObject *c;
        QGraphicsItem *g;
        foreach(c, children) {
            if (c == this) continue;
            g = dynamic_cast<QGraphicsItem*>(c);
            if (g) {
                if (g->sceneBoundingRect().contains(pos)) {
                    QGraphicsScene *s = scene();
                    handled = s->sendEvent(g, e);
                    qDebug() << g->sceneBoundingRect();
                    break;
                }
            }
        }
        qDebug() << "GraphicsSceneMouseEvent "
                 << ((handled)?("handled"):("ignored"));
        handled = true;
    }   break;
    default:
        cout << ((int)(e->type())) << " unhandled" << endl;
        handled = false;
    }
    return handled;
}

bool EventRouter::acceptTouchEvents() const
{
    return true;
}

Qt::MouseButtons EventRouter::acceptedMouseButtons() const
{
    return Qt::LeftButton | Qt::RightButton | Qt::MidButton |
        Qt::MiddleButton | Qt::XButton1 | Qt::XButton2;
}
