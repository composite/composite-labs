/*                                      -*- mode:javascript -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Fluid mouse/touch routing is important to Composite.  This is a
 * test to see if I can do that sort of routing within a QML
 * framework.
 */
import QtQuick 1.0
import EventRouter 1.0

Rectangle {
    id: screen
    color: "light gray"
    width: 600; height: 400;

    Rectangle {
        id: left_bar
	color: "gray"
	anchors.left: parent.left
	width: 32;
	height: parent.height;
    }

    Rectangle {
        id: right_bar
	color: "blue"
	anchors.right: parent.right
	width: 125;
	height: parent.height;
    }

    Rectangle {
        id: bottom_bar
	color: "red"
	anchors.left: left_bar.right
	anchors.bottom: parent.bottom
	anchors.right: right_bar.left
	height: 32;
    }

    Rectangle {
        id: main_area
	color: "green"
	anchors.left: left_bar.right
	anchors.top: parent.top
	anchors.right: right_bar.left
	anchors.bottom: bottom_bar.top
	MouseArea {
	    id: foo;
	    anchors.fill: parent;
	    onClicked: {
		console.log("green click");
	    }
	}
    }

    /* This must be last in order to cover all
     * the other components
     */
    EventRouter {
	id: event_router
	anchors.fill: parent
    }

}
