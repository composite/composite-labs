/*
 * Copyright(c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This file is part of Tritium
 *
 * Tritium is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tritium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/**
 * t_RtString.cpp
 *
 * Basic testing for the RtString template class.
 */

#include "RtString.hpp"

// CHANGE THIS TO MATCH YOUR FILE:
#define THIS_NAMESPACE t_TestTemplate
#include "test_macros.hpp"
#include <cstring>

#define STREQ(a,b) CK(strcmp((a),(b))==0)
#define WSTREQ(a,b) CK(wcscmp((a),(b))==0)

namespace THIS_NAMESPACE
{

    struct Fixture
    {
	// SETUP AND TEARDOWN OBJECTS FOR YOUR TESTS.

	Fixture() {}
	~Fixture() {}
    };

} // namespace THIS_NAMESPACE

using namespace Laboratory; // RtString

TEST_BEGIN( Fixture );

TEST_CASE( 010_constructor )
{
    typedef RtString<char,160> String;
    typedef RtString<wchar_t, 36> WString;

    String a("");
    STREQ(a, "");
    CK( a.size() == 0 );

    String str = String("@").arg("hello, world");
    STREQ(str, "hello, world");
    CK( str.size() == 12 );
    BOOST_MESSAGE(str.ref());

    WString w = WString(L"@").arg(L"hello, world");
    WSTREQ(w,L"hello, world");
    CK( w.size() == 12 );

    String b = String("@#@", '#').arg("a").arg("b");
    STREQ(b, "@a@");
    CK( b.size() == 3 );

    String d("@ @@ @");
    d.arg("");
    STREQ(d, " @@ @");
    d.arg("");
    STREQ(d, " @ @");
    d.arg("");
    STREQ(d, "  @");
    d.arg("");
    STREQ(d, "  ");

}

TEST_CASE( 020_string_substitutions )
{
    typedef RtString<char,6> sm_string;

    sm_string a("@");
    CK( a.size() == 1 );
    a.arg("123456");
    CK( a.size() == 6 );
    STREQ(a, "123456");
    CK( !a.overflow() );

    sm_string b("@");
    CK( b.size() == 1 );
    b.arg("1234567");
    STREQ(b, "123456");
    CK( b.size() == 6 );
    CK( b.overflow() );

    sm_string c("$$$", '$');
    CK( c.size() == 3 );
    c.arg("1234").arg("5").arg("678");
    STREQ(c, "123456");
    CK( c.size() == 6 );
    CK( c.overflow() );

    sm_string d = sm_string("1234@").arg("abcde");
    CK( d.overflow() );
    STREQ(d, "1234ab");
}

TEST_CASE( 030_int_formatting )
{
    typedef RtString<char,16> String;

    String a = String("@").arg(65535U);
    STREQ(a, "65535");
    CK( !a.overflow() );

    String b = String("@ @").arg(65535U).arg(29);
    STREQ(b, "65535 29");
    CK( !b.overflow() );

    String c = String("qwer@y@zz");
    STREQ(c, "qwer@y@zz");
    c.arg(-255);
    STREQ(c, "qwer-255y@zz");
    c.arg(0);
    STREQ(c, "qwer-255y0zz");
    CK( !c.overflow() );

    String d = String("abcdefghijklmno@").arg(-192);
    STREQ(d, "abcdefghijklmno-");
    CK( d.overflow() );
}

TEST_CASE( 040_float_formatting )
{
    typedef RtString<char,160> String;

    String a = String("@").arg(256.123e-12);
    STREQ(a, "256.1230E-12");
    CK( !a.overflow() );

    String b = String("@ @").arg(-1.31498e16).arg(NAN);
    STREQ(b, "-13.1498E15 NaN");
    CK( !b.overflow() );

    String c = String("@ @ @").arg(NAN).arg(INFINITY).arg(-INFINITY);
    CK( (0==strcmp(c, "NaN inf -inf")) || (0==strcmp(c, "NaN inf inf")) );
    CK( !c.overflow() );

    RtString<char,4> d = RtString<char,4>("@").arg(-5.3e-7);
    STREQ(d, "-530");
    CK( d.overflow() );

    String e = String("$", '$').arg(-5.3e-7);
    STREQ(e, "-530.0000E-9");
    CK( !e.overflow() );
}

TEST_END()
