/* -*- coding: utf-8; -*-
 */
#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>

#include "RtString.hpp"
#include "RingBuffer.hpp"

using namespace std;

typedef Laboratory::RtString<char, 140> LogString;
typedef Laboratory::RtString<wchar_t, 140> WLogString;

const uint32_t LOG_BUF = 32768;

Tritium::RingBuffer<char> log_buffer(LOG_BUF);
bool shutdown = false;

void* logger_thread(void* arg)
{
    const uint32_t MAX_BUF = 2048;
    char buf[MAX_BUF];
    uint32_t bytes, pos;

    while( ! shutdown ) {
	if( log_buffer.read_space() > 0 ) {
	    bytes = log_buffer.read( buf, MAX_BUF );
	    assert( bytes < MAX_BUF );
	    pos = 0;
	    cout << "Bytes = " << bytes << endl;
	    while(pos < bytes) {
		if(buf[pos] == '\0') buf[pos] = '\n';
		++pos;
	    }
	    if(pos) buf[pos-1] = '\0';
	    if(bytes) {
		cout << buf << endl;
	    }
	} else {
	    usleep(30000);
	}
    }
    return 0;
}

void log(const char* msg)
{
    size_t len = 0;
    while( msg[len] != '\0' ) ++len;
    assert( log_buffer.write_space() > len );
    log_buffer.write(const_cast<char*>(msg), len+1);
}

void log(LogString& msg)
{
    assert( log_buffer.write_space() > msg.size() );
    log_buffer.write(msg.ref(), msg.size()+1);
}

int main(void)
{
    // Simple examples of using LogString
    LogString foo("Foo!");
    cout << foo << endl;
    LogString bar("Bar @ None");
    bar.arg(9);
    cout << bar << endl;
    LogString bat = LogString("How many? @ ").arg(6553.56199e-7);
    cout << bat << endl;
    LogString bat2 = LogString("More? @ ").arg(-53429.1274e7);
    cout << bat2 << endl;
    WLogString intl = WLogString(L"Está @ veces").arg(125);
    wcout << intl.ref() << endl;
    LogString count = LogString("# # #", '#').arg(6).arg(-36).arg(216);
    cout << count << endl;
    LogString name = LogString("I, @, am @").arg("Sparticus").arg("Superman");
    cout << name << endl;

    cout << endl << "Starting the thread..." << endl << endl;

    pthread_t log_thd;
    int rv;
    rv = pthread_create(&log_thd, 0, logger_thread, 0);
    assert(rv == 0);

    sleep(1);

    for(int n=0 ; n<1000 ; ++n ) {
	log("This is a literal string");
	log(LogString("This is @ dynamic string").arg(1));
	log(LogString("@ for @").arg(5).arg(6));
	usleep(5000);
    }

    sleep(2);
    shutdown = true;

    rv = pthread_join(log_thd, 0);
    assert(rv == 0);

    return 0;
}

