#include "util/stopwatch.hpp"
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <QString>
#include <cstring>
#include <cstdio>
#include "RtString.hpp"
#include <boost/format.hpp>

using namespace std;

const unsigned MAX_ITER = 100000;
const unsigned OBUF_SIZE = 2048;

typedef Laboratory::RtString<char,160> LogString;

inline double rand_f()
{
    double num = rand();
    double den = rand();
    if( rand()%2 ) num = -num;
    if(den == 0) den = 1;
    return (num/den);
}

int main(void)
{
    Laboratory::Stopwatch sw;
    double rt_el, std_el, qs_el, fmt_el, c_el;
    char buf[OBUF_SIZE];

    // INTEGER FORMATTING
    // ===========================================

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	LogString tmp = LogString("Foo #@ (FTW!)").arg(rand());
	strncpy(buf, tmp.ref(), OBUF_SIZE);
    }
    rt_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	ostringstream os;
	os << "Foo #" << rand() << " (FTW!)";
	string tmp = os.str();
	strncpy(buf, tmp.c_str(), OBUF_SIZE);
    }
    std_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	QString tmp = QString("Foo #%1 (FTW!)").arg(rand());
	strncpy(buf, tmp.toLocal8Bit().data(), OBUF_SIZE);
    }
    qs_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	string tmp = str( boost::format("Foo #%1% (FTW!)") % rand() );
	strncpy(buf, tmp.c_str(), OBUF_SIZE);
    }
    fmt_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	char tmp[160];
	snprintf(tmp, 160*sizeof(char), "Foo #%d (FTW!)", rand());
	strncpy(buf, tmp, OBUF_SIZE);
    }
    c_el = sw.stop();
    cout << buf << endl;

    cout << "Results for integer formatting (seconds for "
	 << MAX_ITER << " iterations)" << endl;
    cout << "     RtString: " << rt_el << endl;
    cout << "  std::string: " << std_el << endl;
    cout << "      QString: " << qs_el << endl;
    cout << "boost::format: " << fmt_el << endl;
    cout << "    C strings: " << c_el << endl;
    cout << endl;

    // FLOAT FORMATTING
    // ===========================================
    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	LogString tmp = LogString("Foo #@ (FTW!)").arg(rand_f());
	strncpy(buf, tmp.ref(), OBUF_SIZE);
    }
    rt_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	ostringstream os;
	os << "Foo #" << rand_f() << " (FTW!)";
	string tmp = os.str();
	strncpy(buf, tmp.c_str(), OBUF_SIZE);
    }
    std_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	QString tmp = QString("Foo #%1 (FTW!)").arg(rand_f());
	strncpy(buf, tmp.toLocal8Bit().data(), OBUF_SIZE);
    }
    qs_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	string tmp = str( boost::format("Foo #%1% (FTW!)") % rand_f() );
	strncpy(buf, tmp.c_str(), OBUF_SIZE);
    }
    fmt_el = sw.stop();
    cout << buf << endl;

    srand(12345);
    sw.start();
    for(unsigned int i=0 ; i < MAX_ITER ; ++i) {
	char tmp[160];
	snprintf(tmp, 160*sizeof(char), "Foo #%e (FTW!)", rand_f());
	strncpy(buf, tmp, OBUF_SIZE);
    }
    c_el = sw.stop();
    cout << buf << endl;

    cout << "Results for float formatting (seconds for "
	 << MAX_ITER << " iterations)" << endl;
    cout << "     RtString: " << rt_el << endl;
    cout << "  std::string: " << std_el << endl;
    cout << "      QString: " << qs_el << endl;
    cout << "boost::format: " << fmt_el << endl;
    cout << "    C strings: " << c_el << endl;
    cout << endl;

    return 0;
}
