/*
 * Copyright (c) 2009 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __RTSTRING__HPP__
#define __RTSTRING__HPP__

#include <cstring>
#include <cmath>
#include <cassert>
#include <iostream>
#include <ostream>

namespace Laboratory
{
    /**
     * \brief A real-time safe dynamic string
     *
     * This provides a small, fast string class with dynamic parameter
     * substitution.  All memory for the string is allocated on the
     * stack, therefore there is a compile-time maximum length to any
     * string instance (e.g. 160 characters).  This class is ideally
     * suited to provide rich logging from a thread with real-time
     * priority.
     *
     * Typical usage would be as follows:
     *
     * \code
     *   typedef RtString<char,160> LogString;
     *   logger_function( LogString("Need @ toasters").arg(15) );
     * \endcode
     *
     * This would resolve to your logger as:
     *
     * \code
     *   "Need 15 toasters"
     * \endcode
     *
     * If the user makes the string too long, the message will be
     * truncated rather than overflow the string.  However, the
     * overflow flag will be set, and can be checked by using
     * overflow().
     *
     * \tparam CharT The character type for the continer.
     * \tparam Max   The maximum length of a string.
     */
    template <typename CharT, size_t Max>
    class RtString
    {
    public:
	typedef CharT value_type;  ///< The data type stored in this container.
	const size_t max_chars;    ///< Maximum possible length of a string.
	const size_t npos;         ///< Search result yielded no hits.

	RtString(const CharT* msg, CharT marker = '@');
	~RtString() {}

	RtString& arg(unsigned i, size_t min_digits = 1);
	RtString& arg(int i, size_t min_digits = 1);
	RtString& arg(const CharT* s);
	RtString& arg(double d);

	/**
	 * \brief A pointer to the internal string.
	 */
	// XXX This should not be directly accessible.
	CharT* ref() { return _data; }

	/**
	 * \brief Cast this class to an array of value_type.
	 */
	operator CharT* () { return _data; }

	/**
	 * \brief Return the size of the string.
	 */
	size_t size() { return _size; }
	/**
	 * \brief Return the max. possible size of this string without
	 * overflowing.
	 */
	size_t max_size() { return max_chars; }
	size_t insertion_location();
	/**
	 * \brief True if the string has overflowed.
	 *
	 * When substitiuting parameters, it is possible that the
	 * string will grow beyond max_size().  When this happens, the
	 * string will be truncated, and the overflow flag will be
	 * set.
	 *
	 * \return true if the string has overflowed its maximum size.
	 * false otherwise.
	 */
	bool overflow() { return _overflow; }

    protected:
	// Shift everything over, discarding the _marker
	// Returns the amount of room actually allocated.
	size_t make_room(size_t pos, size_t n);

    private:
	CharT _data[Max + 1];
	size_t _size;
	CharT _marker;
	bool _overflow;
    };

    /**
     * \brief Constructs an RtString with a message.
     *
     * String substitution is done by finding marker and replacing
     * it using one of the arg() methods.  For example:
     *
     * \code
     *    RtString<char,16>("High @").arg(5)
     * \endcode
     *
     * Will replace the '@' with 5, yielding "High 5".
     *
     * \param msg The initial message for the string.
     * \param marker The parameter marker to substitute.
     */
    template <typename CharT, size_t Max>
    RtString<CharT,Max>::RtString(const CharT* msg, CharT marker) : 
	max_chars(Max), 
	npos(-1),
	_size(0),
	_marker(marker),
	_overflow(false)
    {
	assert(max_chars > 0);
	memset(_data, 0, sizeof(_data));
	size_t len = 0;
	while( msg[len] != '\0' ) ++len;
	if( len > max_chars ) {
	    len = max_chars;
	    _overflow = true;
	}
	memcpy( (void*)_data, (void*)msg, len*sizeof(CharT) );
	_size = len;
    }

    /**
     * \brief replace a parameter with an unsigned integer.
     *
     * \param i the integer to insert into the string.
     * \param min_digits the minimum number of digits to
     *          use.  Will be padded with 0's on the left.
     */
    template <typename CharT, size_t Max>
    RtString<CharT,Max>& RtString<CharT,Max>::arg(unsigned i, size_t min_digits)
    {
	if(i==0 && min_digits < 2) {
	    if(min_digits) {
		CharT zero[] = {'0', '\0'};
		arg(zero);
	    }
	    return *this;
	}

	size_t pos;
	size_t digits;
	size_t space;
	size_t d;
	if( i > 0 )
	    digits = 1 + ::floor( ::log10(double(i)) );
	else
	    digits = 1;
	if(digits < min_digits)
	    digits = min_digits;
	pos = insertion_location();
	if( pos != npos ) {
	    space = make_room(pos, digits);

	    if(space < digits) {
		_overflow = true;
		while(digits>space) {
		    i /= 10;
		    --digits;
		}
	    }
	    
	    assert( (pos+digits) < max_chars );

	    // Write the characters backwards.
	    CharT c[] = {'0', '1', '2', '3', '4',
			 '5', '6', '7', '8', '9' };
	    for(pos+=digits-1 ; digits>0 ; --pos, --digits ) {
		d = i % 10;
		_data[pos] = c[d];
		i /= 10;
	    }
	}
	return *this;
    }

    /**
     * \brief replace a parameter with a signed integer.
     *
     * \param i the integer to insert into the string.
     * \param min_digits the minimum number of digits to
     *          use.  Will be padded with 0's on the left.
     *          The '-' sign is not counted.
     */
    template <typename CharT, size_t Max>
    RtString<CharT,Max>& RtString<CharT,Max>::arg(int i, size_t min_digits)
    {
	if(i < 0) {
	    const CharT sign[] = { '-', _marker, '\0' };
	    arg(sign);
	    i = -i;
	}
	arg( (unsigned)i, min_digits );
	return *this;
    }

    /**
     * \brief replace a parameter with a string.
     *
     * \param s the string to insert into the string.
     */
    template <typename CharT, size_t Max>
    RtString<CharT,Max>& RtString<CharT,Max>::arg(const CharT* str)
    {
	size_t len=0, pos;
	while( str[len] != '\0' ) ++len;
	pos = insertion_location();
	len = make_room(pos, len);
	if( len )
	    memcpy((void*)&_data[pos], (void*)str, len*sizeof(CharT));
	return *this;
    }

    /**
     * \brief replace a parameter with a floating point number.
     *
     * Numbers are formatted using engineering-style notation.  This
     * is the same as scientific notation, except that the exponent is
     * always a multiple of 3.  Further, there will always be four
     * digits after the decimal place.  (E.g. 3.5179816e-2 would be
     * formatted as 35.1798E-3.)
     *
     * \param d the number to insert into the string.
     */
    template <typename CharT, size_t Max>
    RtString<CharT,Max>& RtString<CharT,Max>::arg(double d)
    {
	if(isnan(d)) {
	    arg( "NaN" );
	} else if (isinf(d)>0) {
	    arg( "inf" );
	} else if (isinf(d)<0) {
	    arg( "-inf" );
	} else {
	    double exp;
	    if( d>0.0 ) {
		exp = ::log10(d);
	    } else {
		exp = ::log10(-d);
	    }
	    exp = ::floor(exp/3.0) * 3.0;
	    double norm = d * pow(10.0, -exp);
	    double i, f;
	    f = modf(norm, &i);
	    f *= 10000.0;
	    if( f<0.0 ) f = -f;

	    // Max size of string should be:
	    // "-999.9999E-999"
	    // Which is 14.
	    RtString<CharT,16> tmp = RtString<CharT,16>("@.@E@");
	    tmp.arg( int(i) );
	    tmp.arg( unsigned(::round(f)), 4 );
	    tmp.arg( int(exp) );

	    arg(tmp.ref());
	}
	return *this;
    }


    /**
     * \brief Find the index of the next parameter marker.
     */
    template <typename CharT, size_t Max>
    size_t RtString<CharT,Max>::insertion_location() {
	size_t ctr;
	for(ctr=0 ; ctr < max_chars ; ++ctr ) {
	    if(_data[ctr] == _marker) break;
	}
	if(ctr == max_chars) ctr = npos;
	return ctr;
    }

    /**
     * \brief Make room to insert n character at index pos.
     *
     * Shifts the string to make room for adding n characters at
     * location pos.  Returns the number of characters that there is
     * actually room for.
     *
     * \param pos index of string where to insert characters.
     * \param n number of characters to insert.
     *
     * \return The number of characters allocated in the string.
     */
    template <typename CharT, size_t Max>
    size_t RtString<CharT,Max>::make_room(size_t pos, size_t n) {
	size_t dest, cpy, oldsize = _size;

	// Special cases
	if( pos >= max_chars ) {
	    _overflow = true;
	    return 0;
	}
	if(n == 1) return n;
	if( pos >= _size) {
	    dest = pos + n;
	    if( dest < max_chars )
		return n;
	    else
		return (max_chars - pos - 1);
	}
	if(n == 0) {
	    if(_size > 1) {
		cpy = _size - pos - 1;
		memmove( (void*)&_data[pos], (void*)&_data[pos+1], cpy*sizeof(CharT));
		--_size;
	    }
	    _data[_size] = '\0';
	    return 0;
	}

	// Typical case
	dest = pos + n;
	_size += n - 1;
	--n; ++pos; // Consume the @ placemarker
	if( _size > max_chars ) {
	    cpy = n - (_size - max_chars);
	    _size = max_chars;
	    _overflow = true;
	} else {
	    cpy = oldsize - pos;
	}
	if(dest < _size) {
	    cpy = _size - dest;
	    memmove((void*)&_data[dest], (void*)&_data[pos], cpy*sizeof(CharT));
	    _data[_size] = '\0';
	    return 1 + dest - pos;
	}
	if(dest > max_chars) {
	    _overflow = true;
	    dest = max_chars;
	}
	_data[_size] = '\0';
	return 1 + dest - pos;
    }

} // namespace Laboratory

template <typename CharT, size_t Max>
std::ostream& operator<<(std::ostream& os, Laboratory::RtString<CharT,Max>& str)
{
    os << str.ref();
    return os;
}

#endif // __RTSTRING__HPP__
