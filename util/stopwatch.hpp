#ifndef COMPOSITE_LABS_STOPWATCH_H
#define COMPOSITE_LABS_STOPWATCH_H

#include "tnt_stopwatch.h"

namespace Laboratory
{
    typedef TNT::Stopwatch Stopwatch;
}

#endif // COMPOSITE_LABS_STOPWATCH_H
