/**
 * Sequencer.hpp
 *
 */
#ifndef __MIXERTESTS_SEQUENCER_HPP_
#define __MIXERTESTS_SEQUENCER_HPP_

#include <deque>
#include <vector>
#include <stdint.h>
#include "track_data.hpp" // for track_data_seq_node_t

namespace MixerTests
{
    class Sequencer;

    /**
     * This sequencer is a glorified array. :-)
     *
     * In this experiment, the mixer objects control when and
     * where the audio samples get mixed.  So, this is more
     * of a standard way to access the data.
     */
    class Sequencer
    {
    public:
	typedef track_data_seq_node_t node_t;
	typedef std::vector<node_t> track_t;
	typedef std::deque<track_t> collection_t;
	typedef std::vector<bool> bits_t;
	typedef std::vector<uint8_t> heads_t;

	Sequencer();
	~Sequencer();

	void load_all_sequences();

	// Current location of "play head"
	uint32_t location() { return _pos; }
	// One past the end of the sequence.
	uint32_t end() { return _end; };

	// Queues search heads to optimize at pos.  This is
	// sort of like a play head.  Basically this says,
	// "we will not search for /anything/ before this point
	// for a while.
	void locate(uint32_t pos);

	int tracks() { return _seq.size(); }
	bits_t tracks_playing_in_range(uint32_t start, uint32_t end);
	// Give me the first node AFTER where node.end < frame
	// Returns true if one is found, false otherwise.
	bool next(int track, uint32_t frame, node_t& node);

    private:
	collection_t _seq;
	heads_t _heads;
	uint32_t _pos;
	uint32_t _end;
    };

    // This is just to match the way we load all
    // the other objects:
    inline void load_all_sequences(Sequencer& seq) {
	seq.load_all_sequences();
    }

} // namespace MixerTests

#endif // __MIXERTESTS_SEQUENCER_HPP_
