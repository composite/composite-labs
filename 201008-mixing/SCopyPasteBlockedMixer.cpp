/**
 * SCopyPasteBlockedMixer.cpp
 *
 */

#include "SCopyPasteBlockedMixer.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>
#include "mixing_subroutines.hpp"

#include <sys/time.h>
#include <sys/resource.h>

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    SCopyPasteBlockedMixer::SCopyPasteBlockedMixer() :
	_name("SCopyPasteBlockedMixer"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string SCopyPasteBlockedMixer::name() {
	return _name;
    }

    void SCopyPasteBlockedMixer::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void SCopyPasteBlockedMixer::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void SCopyPasteBlockedMixer::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);

	assert( _name.length() != 0 );
	uint32_t n_tracks = ((_tracks->size()/8)+1)*8;
	uint32_t n_cache_min = n_tracks * nframes * sizeof(float);

	if( _gains.size() < n_tracks ) {
	    _gains.clear();
	    _gains.insert( _gains.end(), n_tracks, 0.0f );
	    _cache.clear();
	    _cache.insert( _cache.end(), n_tracks, 0 );
	}
	if( _cache_buf.size() < n_cache_min ) {
	    _cache_buf.clear();
	    _cache_buf.insert( _cache_buf.end(), n_cache_min, 0.0f );
	}
	for( unsigned k = 0 ; k < n_tracks ; ++k ) {
	    _cache[k] = &_cache_buf[k*nframes];
	}
    }

    void SCopyPasteBlockedMixer::pre_process()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int track, n_tracks = _seq->tracks();
	int n_cache = ((n_tracks / 8) + 1) * 8;
	uint32_t start, end, track_start, f, frames, samp_off;
	bool res;
	Sequencer::bits_t playing;
	Sequencer::node_t node;

	assert( _cache_buf.size() > (n_cache * _nframes) );
	memset(&_cache_buf[0], 0, n_cache * _nframes * sizeof(float));
	memset(&_gains[0], 0, n_tracks * sizeof(float));

	start = _pos;
	end = _pos + _nframes;

	_seq->locate(start);
	playing = _seq->tracks_playing_in_range(start, end);

	float mixer_gain = 1.0 / static_cast<float>(n_tracks);

	for( track = 0 ; track < n_tracks ; ++track ) {
	    if( ! playing[track] ) continue;
	    track_start = start;
	    while(track_start < end) {
		DLOG( if( track_start != start ) cout << "wraparound..." << endl );
		res = _seq->next(track, track_start, node);
		if( !res ) {
		    track_start = end;
		    continue;
		}
		if( node.start >= end ) {
		    track_start = end;
		    continue;
		}
		// Clip the node
		if( node.end > end ) {
		    node.end = end;
		}
		if( node.start < track_start ) {
		    node.offset += (track_start - node.start);
		    node.start = track_start;
		}
		samp_off = node.offset;
		frames = node.end - node.start;
		assert( frames > 0 );
		assert( frames <= (end - track_start) );
		assert( frames <= (end - start) );
		assert( samp_off < _tracks->at(track).size() );

		_gains[track] = node.gain * mixer_gain; // mixer_gain... divide output by number of tracks
		f = track_start - start;
		DLOG( cout << "@" << _pos << " -- "
		      << "copy to cache[" << track << "][" << f << "] "
		      << "from track + " << samp_off
		      << " (" << frames << " frames" << endl );
		Subroutines::copy_buffer_apply_gain( &_cache[track][f],
						     _tracks->at(track).data(samp_off),
						     frames,
						     _gains[track] );
		track_start += frames;
	    }
	}
    }

    void SCopyPasteBlockedMixer::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	/*
	memset(_out, 0, _nframes*sizeof(float));
	for(unsigned k=0 ; k<_tracks->size() ; ++k ) {
	    Subroutines::add_buffer( _out, _cache[k], _nframes);
	}
	*/
	Subroutines::mix_down_partitioned_buffer( _out,
						  &_cache_buf[0], _nframes,
						  _tracks->size() );

    }

    void SCopyPasteBlockedMixer::post_process()
    {}

} // namespace MixerTests
