/**
 * Track.hpp
 *
 * This is generic access to track data
 */
#ifndef __MIXERTESTS_TRACK_HPP__
#define __MIXERTESTS_TRACK_HPP__

#include <memory>
#include <vector>
#include <stdint.h>

namespace MixerTests
{
    class Track
    {
    public:
	Track();
	~Track();

	uint32_t size() { return _data.size(); }
	float* data(uint32_t offset = 0) { return &_data[offset]; };
	uint32_t remaining( uint32_t offset ) {
	    return size() - offset;
	}
	float sum() { return _sum; }

	void load_track(int id);

    protected:
	void calc_sum();

    private:
	std::vector<float> _data;
	float _sum;
    };

} // MixerTests

#endif // __MIXERTESTS_TRACK_HPP__
