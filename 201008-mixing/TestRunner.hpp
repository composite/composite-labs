/**
 * TestRunner.hpp
 *
 * This object will run all the tests and report the results.
 */
#ifndef __MIXERTESTS_TESTRUNNER_HPP__
#define __MIXERTESTS_TESTRUNNER_HPP__

#include <deque>
#include <stdint.h>

namespace MixerTests
{
    class Mixer;
    class Track;
    class Sequencer;

    class TestRunner
    {
    public:
	typedef std::deque<Track> tracks_t;

	TestRunner();
	~TestRunner();

	void set_sequencer(Sequencer* seq);
	void set_mixer(Mixer* mix);
	void set_tracks(tracks_t* tracks); 
	void set_master_mixdown(float *mix, uint32_t size);
	void run_test(uint32_t nframes);
	void report();
			
    private:
	Sequencer* _seq;
	Mixer* _mix;
	tracks_t* _tracks;

	uint32_t _pre_clocks;
	uint32_t _mix_clocks;
	uint32_t _post_clocks;
	float _out_sum;
	std::deque<float> _master_mixdown;
    };

} // namespace Mixer

#endif // __MIXER_TESTRUNNER_HPP__
