/**
 * ReferenceMixerImpl.cpp
 *
 */

#include "ReferenceMixerImpl.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    ReferenceMixerImpl::ReferenceMixerImpl() :
	_name("ReferenceMixerImpl"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string ReferenceMixerImpl::name() {
	return _name;
    }

    void ReferenceMixerImpl::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void ReferenceMixerImpl::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void ReferenceMixerImpl::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);
    }

    void ReferenceMixerImpl::pre_process()
    {}

    void ReferenceMixerImpl::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int n_tracks = _seq->tracks();
	int track;
	uint32_t start, end, track_start, frame, sample;
	bool res;
	Sequencer::node_t node;
	float buf[_nframes];

	memset(_out, 0, _nframes*sizeof(float));
	start = _pos;
	end = _pos + _nframes;
	DLOG( cout << _pos << endl );

	_seq->locate(0);
	for( track = 0 ; track < n_tracks ; ++track ) {

	    track_start = start;
	    for( frame = 0 ; frame < _nframes ; ++frame ) {
		res = _seq->next(track, track_start + frame, node);
		if( !res ) {
		    DLOG( cout << "Zero" << endl );
		    buf[frame] = 0.0f;
		    continue;
		}
		assert(node.end > (track_start + frame));
		if( node.start > (track_start + frame) ) {
		    DLOG( cout << "Zero" << endl );
		    buf[frame] = 0.0f;
		    continue;
		}
		if( node.start < (track_start + frame) ) {
		    node.offset += (track_start + frame - node.start);
		    node.start = track_start + frame;
		}
		sample = node.offset;
		buf[frame] = node.gain * *(_tracks->at(track).data(sample));
	    }
	    float mixer_gain = 1.0f / static_cast<float>(n_tracks);
	    for( frame = 0 ; frame < _nframes ; ++frame ) {
		_out[frame] += buf[frame] * mixer_gain;
	    }
	}
    }

    void ReferenceMixerImpl::post_process()
    {}

} // namespace MixerTests
