/**
 * mixing_subroutines.hpp
 *
 * Basic, atomic mixing subs
 */
#ifndef __MIXERTESTS_MIXING_SUBROUTINES_HPP__
#define __MIXERTESTS_MIXING_SUBROUTINES_HPP__

#include <stdint.h>
#include <cstring>

namespace MixerTests
{
    namespace Subroutines
    {
	typedef float sample_t;

	inline void copy_buffer(sample_t* dest, sample_t* src, uint32_t n)
	{
	    memcpy(dest, src, n * sizeof(sample_t));
	}

	void add_buffer(sample_t* dest, sample_t* src, uint32_t n);
	void add_buffer_apply_gain(sample_t* dest, sample_t* src,
				   uint32_t n, sample_t gain);
	void copy_buffer_apply_gain(sample_t* dest, sample_t* src,
				   uint32_t n, sample_t gain);
	inline void zero_buffer(sample_t* dest, uint32_t n)
	{
	    memset(dest, 0, n*sizeof(sample_t));
	}

	void clip_buffer(sample_t* buf, uint32_t n);

	void copy_buffers_apply_gain_4( sample_t* dest, uint32_t n,
					sample_t* s0, sample_t g0,
					sample_t* s1, sample_t g1,
					sample_t* s2, sample_t g2,
					sample_t* s3, sample_t g3 );
	void add_buffers_apply_gain_4( sample_t* dest, uint32_t n,
					sample_t* s0, sample_t g0,
					sample_t* s1, sample_t g1,
					sample_t* s2, sample_t g2,
					sample_t* s3, sample_t g3 );
	void mix_down_partitioned_buffer( sample_t *dest, sample_t* src, uint32_t nframes,
					  uint32_t count );
	void apply_gain( sample_t *dest, uint32_t nframes, float gain );

	namespace SIMD
	{
	    typedef float __vf4 __attribute__((vector_size((16))));

	    typedef union {
		float f[4];
		__vf4 v;
	    } vf4;

	    inline bool aligned_16(void * ptr ) { return (0 == (((int)ptr) & 0xfL)); }

	    void copy_buffer( vf4 *dest, vf4 *src, uint32_t n,
			      void* d_bound = 0, void* s_bound = 0);
	    void copy_buffer_apply_gain( vf4 *dest, vf4 *src,
					 uint32_t n, float gain,
					 void* d_bound = 0, void* s_bound = 0);
	    void add_buffer_apply_gain( vf4 *dest, vf4 *src,
					uint32_t n, float gain,
					void* d_bound = 0, void *s_bound = 0);
	    void add_buffer( vf4 *dest, vf4 *src,
			     uint32_t n,
			     void *d_bound = 0, void *s_bound = 0 );

	void mix_down_partitioned_buffer( vf4 *dest, vf4* src, uint32_t nframes,
					  uint32_t count );
	} // namespace SIMD

    } // namespace Subroutines
} // namespace MixerTests

#endif // __MIXERTESTS_MIXING_SUBROUTINES_HPP__
