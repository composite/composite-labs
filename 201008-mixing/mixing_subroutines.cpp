/**
 * mixing_subroutines.cpp
 *
 */

#include "mixing_subroutines.hpp"
#include <cassert>

namespace MixerTests
{
    namespace Subroutines
    {
	void add_buffer(sample_t* dest, sample_t* src, uint32_t n)
	{
	    // Let the compiler unroll the loop.
	    while(n--) {
		dest[n] += src[n];
	    }
	}

	void add_buffer_apply_gain(sample_t* dest, sample_t* src,
				   uint32_t n, sample_t gain)
	{
	    // Let the compiler unroll the loop.
	    while(n--) {
		dest[n] += src[n] * gain;
	    }
	}

	void copy_buffer_apply_gain(sample_t* dest, sample_t* src,
				   uint32_t n, sample_t gain)
	{
	    assert( n > 7 );
	    assert( 0 == ((n-1) & n) );
	    uint32_t count = n/8;
	    while(count--) {
		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;

		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;
		(*dest++) = (*src++) * gain;
	    }
	}

	void clip_buffer(sample_t* buf, uint32_t n)
	{
	    // Let the compiler unroll the loop.
	    while(n--) {
		if( buf[n] > 1.0f ) {
		    buf[n] = 1.0f;
		} else if( buf[n] < -1.0f ) {
		    buf[n] = -1.0f;
		}
	    }
	}

	void copy_buffers_apply_gain_4( sample_t* dest, uint32_t n,
					sample_t* s0, sample_t g0,
					sample_t* s1, sample_t g1,
					sample_t* s2, sample_t g2,
					sample_t* s3, sample_t g3 )
	{
	    sample_t local[n];

	    if( !s0 || !s1 || !s2 || !s3 ) {
		memset(local, 0, n*sizeof(sample_t));
		if(!s0) s0 = local;
		if(!s1) s1 = local;
		if(!s2) s2 = local;
		if(!s3) s3 = local;
	    }
	    unsigned iters = n/8;
	    assert( n == iters*8 );
	    while(n--) {
		dest[n] = s0[n]*g0 + s1[n]*g1 + s2[n]*g2 + s3[n]*g3;
	    }
	}

	void add_buffers_apply_gain_4( sample_t* dest, uint32_t n,
					sample_t* s0, sample_t g0,
					sample_t* s1, sample_t g1,
					sample_t* s2, sample_t g2,
					sample_t* s3, sample_t g3 )
	{
	    sample_t local[n];

	    if( !s0 || !s1 || !s2 || !s3 ) {
		memset(local, 0, n*sizeof(sample_t));
		if(!s0) s0 = local;
		if(!s1) s1 = local;
		if(!s2) s2 = local;
		if(!s3) s3 = local;
	    }
	    unsigned iters = n/8;
	    assert( n == iters*8 );
	    while(n--) {
		dest[n] += s0[n]*g0 + s1[n]*g1 + s2[n]*g2 + s3[n]*g3;
	    }
	}

	void mix_down_partitioned_buffer( sample_t *dest, sample_t* src, uint32_t nframes,
					  uint32_t count )
	{
	    if( !count ) return;

	    sample_t *o;
	    const unsigned stride = 8;

	    assert( nframes > 8 );
	    assert( ((nframes-1) & nframes) == 0 );

	    unsigned inner_save = nframes/stride;

	    unsigned inner = inner_save;
	    o = dest;
	    while(inner--) {
		(*o++) = (*src++);
		(*o++) = (*src++);
		(*o++) = (*src++);
		(*o++) = (*src++);

		(*o++) = (*src++);
		(*o++) = (*src++);
		(*o++) = (*src++);
		(*o++) = (*src++);
	    }
	    count--;
	    while(count--) {
		o = dest;
		inner = inner_save;
		while(inner--) {
		    (*o++) += (*src++);
		    (*o++) += (*src++);
		    (*o++) += (*src++);
		    (*o++) += (*src++);

		    (*o++) += (*src++);
		    (*o++) += (*src++);
		    (*o++) += (*src++);
		    (*o++) += (*src++);
		}
	    }
	}

	void apply_gain( sample_t *dest, uint32_t nframes, float gain )
	{
	    while(nframes--) {
		dest[nframes] *= gain;
	    }
	}

	namespace SIMD
	{
	    void copy_buffer( vf4 *dest, vf4 *src, uint32_t n,
			      void* d_bound, void* s_bound )
	    {
		assert( aligned_16(dest) );
		assert( aligned_16(src) );
		assert( (n%8) == 0 );
		n /= 8;

		while(n--) {
		    *dest++ = *src++;
		    *dest++ = *src++;
		    *dest++ = *src++;
		    *dest++ = *src++;

		    *dest++ = *src++;
		    *dest++ = *src++;
		    *dest++ = *src++;
		    assert( !dest || (dest < d_bound));
		    assert( !src  || (src < s_bound));
		    *dest++ = *src++;
		}
	    }

	    void copy_buffer_apply_gain( vf4 *dest, vf4 *src, uint32_t n, float gain,
			      void* d_bound, void* s_bound )
	    {
		assert( aligned_16(dest) );
		assert( aligned_16(src) );
		assert( (n%8) == 0 );
		n /= 8;

		vf4 gg;
		gg.f[0] = gg.f[1] = gg.f[2] = gg.f[3] = gain;

		while(n--) {
		    dest->v = src->v * gg.v; ++dest, ++src;
		    dest->v = src->v * gg.v; ++dest, ++src;
		    dest->v = src->v * gg.v; ++dest, ++src;
		    dest->v = src->v * gg.v; ++dest, ++src;

		    dest->v = src->v * gg.v; ++dest, ++src;
		    dest->v = src->v * gg.v; ++dest, ++src;
		    dest->v = src->v * gg.v; ++dest, ++src;
		    assert( !dest || (dest < d_bound));
		    assert( !src  || (src < s_bound));
		    dest->v = src->v * gg.v; ++dest, ++src;
		}
	    }

	    void add_buffer_apply_gain( vf4 *dest, vf4 *src, uint32_t n, float gain,
			      void* d_bound, void* s_bound )
	    {
		assert( aligned_16(dest) );
		assert( aligned_16(src) );
		assert( (n%8) == 0 );
		n /= 8;

		vf4 gg;
		gg.f[0] = gg.f[1] = gg.f[2] = gg.f[3] = gain;

		while(n--) {
		    dest->v += src->v * gg.v; ++dest, ++src;
		    dest->v += src->v * gg.v; ++dest, ++src;
		    dest->v += src->v * gg.v; ++dest, ++src;
		    dest->v += src->v * gg.v; ++dest, ++src;

		    dest->v += src->v * gg.v; ++dest, ++src;
		    dest->v += src->v * gg.v; ++dest, ++src;
		    dest->v += src->v * gg.v; ++dest, ++src;
		    assert( !dest || (dest < d_bound));
		    assert( !src  || (src < s_bound));
		    dest->v += src->v * gg.v; ++dest, ++src;
		}
	    }

	    void add_buffer( vf4 *dest, vf4 *src, uint32_t n,
			      void* d_bound, void* s_bound )
	    {
		assert( aligned_16(dest) );
		assert( aligned_16(src) );
		assert( (n%8) == 0 );
		n /= 8;
		while(n--) {
		    dest->v += src->v; ++dest, ++src;
		    dest->v += src->v; ++dest, ++src;
		    dest->v += src->v; ++dest, ++src;
		    dest->v += src->v; ++dest, ++src;

		    dest->v += src->v; ++dest, ++src;
		    dest->v += src->v; ++dest, ++src;
		    dest->v += src->v; ++dest, ++src;
		    assert( !dest || (dest < d_bound));
		    assert( !src  || (src < s_bound));
		    dest->v += src->v; ++dest, ++src;
		}
	    }

	    void mix_down_partitioned_buffer( vf4 *dest, vf4 *src, uint32_t nframes,
					      uint32_t count )
	    {
		if( !count ) return;

		vf4 *o;
		const unsigned stride = 8;

		assert( nframes > 8 );
		assert( ((nframes-1) & nframes) == 0 );

		unsigned inner_save = nframes/stride;

		unsigned inner = inner_save;
		o = dest;
		while(inner--) {
		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;

		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;
		    o->v = src->v; ++o, ++src;
		}
		count--;
		while(count--) {
		    o = dest;
		    inner = inner_save;
		    while(inner--) {
			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;

			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;
			o->v += src->v; ++o, ++src;
		    }
		}
	    }

	} // namespace SIMD
    } // namespace Subroutines
} // namespace MixerTests

