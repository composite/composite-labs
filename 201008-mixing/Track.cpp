/**
 * Track.cpp
 */

#include "Track.hpp"
#include "track_data.hpp"

namespace MixerTests
{
    Track::Track()
    {}

    Track::~Track()
    {}

    void Track::load_track(int id)
    {
	int N = track_data_get_track_length(id);

	_data.clear();
	_data.assign(N, 0.0f);
	track_data_copy_data(id, &_data[0], N);

	calc_sum();
    }

    void Track::calc_sum()
    {
	float ans = 0.0;
	std::vector<float>::const_iterator it;
	for(it = _data.begin() ; it != _data.end() ; ++it) {
	    ans += (*it);
	}
	_sum = ans;
    }

} // namespace MixerTests
