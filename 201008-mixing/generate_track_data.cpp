/**
 * generate_track_data.cpp
 *
 * Program to auto-generate data for tracks.
 */

#include <cstdio>
#include <string>
#include <sstream>
#include <deque>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdint.h>
#include <cmath>
#include <cassert>
#include <unistd.h>

using namespace std;

#define OPTION_FULL_LENGTH

typedef struct {
    float gain;
    uint32_t offset, start, end;
} track_data_seq_node_t;

typedef deque<uint32_t> uint_vec;
typedef deque<float> float_vec;
typedef deque<double> double_vec;
typedef deque<track_data_seq_node_t> tseq_vec;
typedef deque< tseq_vec > seq_vec;

static uint_vec get_track_lengths( int tracks,
				   uint32_t oall_length,
				   uint32_t min,
				   uint32_t max,
				   uint32_t len,
				   uint32_t sample_rate )
{
    uint_vec lens;
    uint32_t tmp;

    min *= sample_rate;
    max *= sample_rate;
    len *= sample_rate;

    uint32_t diff = max - min;

    while(tracks--) {
#ifndef OPTION_FULL_LENGTH
	tmp = rand() % diff;
	lens.push_back( min + tmp );
#else
	lens.push_back( oall_length );
#endif
    }

    return lens;
}

static void generate_one_track( float_vec& buf,
				uint32_t length,
				uint32_t sample_rate )
{
    // Generate a size wave with zero crossovers.
    const double freq_min = 40.0; // Hz
    const double freq_max = 2000.0; // Hz

    double time = double(length) / double(sample_rate);
    uint32_t mmin, mmax, mult;

    mmin = ceil( time * freq_min );
    mmax = floor( time * freq_max );
    assert( mmax - mmin );

    mult = mmin + (rand() % (mmax - mmin));

    double freq = double(mult) / time;
    double tmp;
    double check_sum = 0.0;
    uint32_t k;

    for( k = 0 ; k < length ; ++k ) {
	tmp = sin( 2.0 * M_PI * freq * double(k) / double(sample_rate) );
	buf.push_back( tmp );
	check_sum += tmp;
    }
    assert( fabs(check_sum) < 1e-5 );

    cout << freq << " ";
}

static void generate_track_data( int tracks,
				 deque< float_vec >& track_data,
				 uint_vec lengths,
				 uint32_t sample_rate )
{
    cout << "Frequencies: ";
    while(tracks--) {
	track_data.push_back( float_vec() );
	generate_one_track( track_data.back(),
			    lengths[ track_data.size() - 1],
			    sample_rate );
    }
    cout << endl;
}

static void write_data_to_file( float_vec& data, string fn )
{
	FILE *of = fopen(fn.c_str(), "wb");
	assert(of);
	int fd = fileno(of);

	const uint32_t stride = 1024;
	const uint32_t mask = stride - 1;
	float buf[stride];
	uint32_t k, written, have, w, f;

	written = 0;
	f = 0;
	while( written < data.size() * sizeof(float) ) {
	    for( k=0 ; k<stride && f<data.size() ; ++k, ++f ) {
		buf[k] = data[f];
	    }
	    have = k * sizeof(float);
	    w = 0;
	    while( w < have ) {
		w += write( fd, &buf[w/sizeof(float)], have );
	    }
	    written += have;
	}
	fclose(of);
}

static deque<string> write_sample_files( deque< float_vec >& track_data )
{
    int N = track_data.size();
    int k;
    deque<string> names;
    string name;

    for( k = 0 ; k < N ; ++k ) {
	stringstream ss;
	ss << "data_sample_" << k << ".dat";
	name = ss.str();
	cout << name << endl;
	write_data_to_file( track_data[k], name );
	names.push_back(name);
    }
    return names;
}

static float rand_uniform()
{
    return float(rand()) / float(RAND_MAX);
}

static void sequence_tracks_full_length( deque< float_vec >& track_data,
			     seq_vec& sequence )
{
    int k, N = track_data.size();
    for( k = 0 ; k < N ; ++k ) {
	float gain = 0.5f + (rand_uniform()/2.0f);
	tseq_vec ts;
	track_data_seq_node_t tmp = {gain, 0, 0, track_data.at(k).size() };
	ts.push_back(tmp);
	sequence.push_back(ts);
    }
}

static void sequence_tracks_triggers( deque< float_vec >& track_data,
				      seq_vec& sequence )
{

}

static void sequence_tracks( deque< float_vec >& track_data,
			     seq_vec& sequence )
{
#ifndef OPTION_FULL_LENGTH
    sequence_tracks_triggers(track_data, sequence);
#else
    sequence_tracks_full_length(track_data, sequence);
#endif
}

static string write_sequence( seq_vec& sequence )
{
    const char seq_fn[] = "data_sequence.dat";
    FILE *of = fopen(seq_fn, "wb");
    assert(of);
    int fd = fileno(of);
    seq_vec::iterator t;
    tseq_vec::iterator s;
    for( t = sequence.begin() ; t != sequence.end() ; ++t ) {
	uint16_t size = t->size();
	write( fd, &size, sizeof(uint16_t) );
	for( s = t->begin() ; s != t->end() ; ++s ) {
	    write( fd, &(*s), sizeof(track_data_seq_node_t) );
	}
    }
    fclose(of);

    return string(seq_fn);	
}

static void make_final_mixdown( float_vec& dest,
				deque< float_vec >& track_data,
				uint32_t oall_length,
				seq_vec& sequence,
				double mix_gain )
{
    const unsigned tracks = track_data.size();
    float_vec buf;
    double_vec tmp(oall_length, 0.0);
    uint32_t k, t;

    // Fill buff
    for( t = 0 ; t < tracks ; ++t ) {
	tseq_vec& seq = sequence[t];
	buf.clear();
	buf.assign(oall_length, 0.0f);
	float g;
	uint32_t pos;

	tseq_vec::iterator it = seq.begin();
	for( k = 0 ; k < oall_length && it != seq.end() ; ++k ) {
	    if( k >= it->end ) ++it;
	    if( k < it->start ) continue;
	    g = it->gain;
	    pos = it->offset + (k - it->start);
	    assert( pos >= 0 );
	    assert( pos < track_data[t].size() );
	    buf[k] = g * track_data[t][pos];
	}
	for( k = 0 ; k < oall_length ; ++k ) {
	    tmp[k] += static_cast<double>( buf[k] );
	}
    }

    // Mix tmp (double) to dest (float);
    for( k = 0 ; k < oall_length ; ++k ) {
	dest[k] = static_cast<float>(tmp[k]/mix_gain);
	if(dest[k] > 1.0f) dest[k] = 1.0f;
	if(dest[k] < -1.0f) dest[k] = -1.0f;
    }
}

static string write_final_mixdown( float_vec& data )
{
    const char mix_fn[] = "data_mixdown.dat";
    string fn(mix_fn);
    write_data_to_file(data, fn);
    cout << fn << endl;
    return fn;
}

static void write_config( uint32_t oall_length,
			  deque< string >& track_files,
			  string sequence_file,
			  string mixdown_file,
			  string conf_file_name )
{
    ofstream of;

    of.open(conf_file_name.c_str(), ios::out);

    of << oall_length << endl;
    of << track_files.size() << endl;
    of << sequence_file << endl;
    of << mixdown_file << endl;
    deque<string>::iterator it;
    for( it = track_files.begin() ; it != track_files.end() ; ++it ) {
	of << (*it) << endl;
    }
    of.close();
}

int main(void)
{
    // CONFIGURATION
    const char CONFIG[] = "track_data.conf";
    const int tracks = 21;
    const uint32_t sample_rate = 48000;
    const float longest_track = 10.0f; // seconds
    const float shortest_track = 3.0f; // seconds
    const float song_length = 60.0f; // seconds
    // END CONFIGURATION

    uint32_t oall_length;

    srand( 0x3AAD3371L );

    oall_length = song_length * sample_rate;
    cout << oall_length << endl;

    uint_vec lengths = get_track_lengths( tracks,
					  oall_length,
					  shortest_track,
					  longest_track,
					  song_length,
					  sample_rate );

    for(uint_vec::iterator it = lengths.begin() ;
	it != lengths.end() ;
	++it ) {
	cout << (*it) << " ";
    }
    cout << endl;
    
    deque< float_vec > track_data;
    generate_track_data( tracks,
			 track_data,
			 lengths,
			 sample_rate );

    std::deque<string> names = write_sample_files( track_data );

    seq_vec sequence;

    sequence_tracks( track_data, sequence );

    string s_file = write_sequence( sequence );
    cout << s_file << endl;

    float_vec mixdown(oall_length, 0.0f);
    make_final_mixdown( mixdown, track_data, oall_length, sequence, double(tracks) );
    string m_file = write_final_mixdown( mixdown );

    write_config( oall_length,
		  names,
		  s_file,
		  m_file,
		  CONFIG );

    return 0;
}
