/**
 * Mixer.cpp
 *
 */

#include <cassert>

#include "Mixer.hpp"
#include "ReferenceMixerImpl.hpp"
#include "CopyPasteMixerImpl.hpp"
#include "PortStyleMixerImpl.hpp"
#include "BlockedPortStyleMixerImpl.hpp"
#include "SplitCopyPasteMixerImpl.hpp"
#include "SCopyPaste4MixerImpl.hpp"
#include "SCopyPasteBlockedMixer.hpp"
#include "SCopyPasteBlockedMixerSIMD.hpp"
#include "BlockedArdourMixer.hpp"

namespace MixerTests
{
    void load_all_mixers(std::deque<Mixer*>& mixers)
    {
	Mixer* tmp;
	if(true) {
	    ReferenceMixerImpl* m = new ReferenceMixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    CopyPasteMixerImpl* m = new CopyPasteMixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    PortStyleMixerImpl* m = new PortStyleMixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    BlockedPortStyleMixerImpl* m = new BlockedPortStyleMixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    SplitCopyPasteMixerImpl* m = new SplitCopyPasteMixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    SCopyPaste4MixerImpl* m = new SCopyPaste4MixerImpl;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    SCopyPasteBlockedMixer* m = new SCopyPasteBlockedMixer;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    SCopyPasteBlockedMixerSIMD* m = new SCopyPasteBlockedMixerSIMD;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
	if(true){
	    BlockedArdourMixer* m = new BlockedArdourMixer;
	    assert(m);
	    tmp = dynamic_cast<Mixer*>(m);
	    assert(tmp);
	    mixers.push_back( tmp );
	}
    }

    void delete_all_mixers(std::deque<Mixer*>& mixers)
    {
	std::deque<Mixer*>::iterator it;
	for( it = mixers.begin() ; it != mixers.end() ; ++it ) {
	    delete (*it);
	}
	mixers.clear();
    }

} // MixerTests
