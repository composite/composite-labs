/*
    Copyright (C) 2005 Sampo Savolainen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
#ifndef __ardour_mix_h__
#define __ardour_mix_h__

extern "C" {
/* SSE functions */
    float x86_sse_compute_peak         (float * buf, uint32_t nsamples, float current);
    void  x86_sse_apply_gain_to_buffer (float * buf, uint32_t nframes, float gain);
    void  x86_sse_mix_buffers_with_gain(float * dst, const float * src, uint32_t nframes, float gain);
    void  x86_sse_mix_buffers_no_gain  (float * dst, const float * src, uint32_t nframes);
}

#endif
