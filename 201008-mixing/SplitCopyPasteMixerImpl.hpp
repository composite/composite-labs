/**
 * SplitCopyPasteMixerImpl.hpp
 *
 * A reference mixing implementation.  Emphasis is on accuracy rather
 * than speed.
 */
#ifndef __MIXERTESTS_SPLITCOPYPASTEMIXERIMPL_HPP_
#define __MIXERTESTS_SPLITCOPYPASTEMIXERIMPL_HPP_

#include "Mixer.hpp"
#include <vector>

namespace MixerTests
{
    /**
     * Mix buffers by copying chunks to the stack.
     */
    class SplitCopyPasteMixerImpl : public Mixer
    {
    public:
	SplitCopyPasteMixerImpl();
	virtual ~SplitCopyPasteMixerImpl() {}

	/* Non-timed routines */
	virtual std::string name();
	virtual void set_sequencer(Sequencer* seq);
	virtual void set_tracks(std::deque<Track>* tracks);
	virtual void locate(uint32_t frame, uint32_t nframes, float* out);

	/* Routines that will be timed */
	virtual void pre_process();
	virtual void mix();
	virtual void post_process();

    private:
	std::string _name;
	uint32_t _pos;
	uint32_t _nframes;
	float* _out;
	Sequencer* _seq;
	std::deque<Track>* _tracks;

	std::vector<float> _cache_buf;
	std::vector<float*> _cache;
	std::vector<float> _gains;

    };

} // namespace MixerTests

#endif // __MIXERTESTS_SPLITCOPYPASTEMIXERIMPL_HPP_
