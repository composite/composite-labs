/**
 * SCopyPaste4MixerImpl.cpp
 *
 */

#include "SCopyPaste4MixerImpl.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>
#include "mixing_subroutines.hpp"

#include <sys/time.h>
#include <sys/resource.h>

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    SCopyPaste4MixerImpl::SCopyPaste4MixerImpl() :
	_name("SCopyPaste4MixerImpl"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string SCopyPaste4MixerImpl::name() {
	return _name;
    }

    void SCopyPaste4MixerImpl::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void SCopyPaste4MixerImpl::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void SCopyPaste4MixerImpl::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);

	assert( _name.length() != 0 );
	uint32_t n_tracks = ((_tracks->size()/8)+1)*8;
	uint32_t n_cache_min = n_tracks * nframes * sizeof(float);

	if( _gains.size() < n_tracks ) {
	    _gains.clear();
	    _gains.insert( _gains.end(), n_tracks, 0.0f );
	    _cache.clear();
	    _cache.insert( _cache.end(), n_tracks, 0 );
	}
	if( _cache_buf.size() < n_cache_min ) {
	    _cache_buf.clear();
	    _cache_buf.insert( _cache_buf.end(), n_cache_min, 0.0f );
	}
	for( unsigned k = 0 ; k < n_tracks ; ++k ) {
	    _cache[k] = &_cache_buf[k*nframes];
	}
    }

    void SCopyPaste4MixerImpl::pre_process()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int track, n_tracks = _seq->tracks();
	int n_cache = ((n_tracks / 8) + 1) * 8;
	uint32_t start, end, track_start, f, frames, samp_off;
	bool res;
	Sequencer::bits_t playing;
	Sequencer::node_t node;

	assert( _cache_buf.size() > (n_cache * _nframes) );
	memset(&_cache_buf[0], 0, n_cache * _nframes * sizeof(float));
	memset(&_gains[0], 0, n_tracks * sizeof(float));

	start = _pos;
	end = _pos + _nframes;

	_seq->locate(start);
	playing = _seq->tracks_playing_in_range(start, end);

	float mixer_gain = 1.0 / static_cast<float>(n_tracks);
	for( track = 0 ; track < n_tracks ; ++track ) {
	    if( ! playing[track] ) continue;

	    track_start = start;
	    while(track_start < end) {
		DLOG( if( track_start != start ) cout << "wraparound..." << endl );
		res = _seq->next(track, track_start, node);
		if( !res ) {
		    track_start = end;
		    continue;
		}
		if( node.start >= end ) {
		    track_start = end;
		    continue;
		}
		// Clip the node
		if( node.end > end ) {
		    node.end = end;
		}
		if( node.start < track_start ) {
		    node.offset += (track_start - node.start);
		    node.start = track_start;
		}
		samp_off = node.offset;
		frames = node.end - node.start;
		assert( frames > 0 );
		assert( frames <= (end - track_start) );
		assert( frames <= (end - start) );
		assert( samp_off < _tracks->at(track).size() );

		_gains[track] = node.gain * mixer_gain; // mixer_gain... divide output by number of tracks
		f = track_start - start;
		DLOG( cout << "@" << _pos << " -- "
		      << "copy to cache[" << track << "][" << f << "] "
		      << "from track + " << samp_off
		      << " (" << frames << " frames" << endl );
		memcpy( &_cache[track][f],
			_tracks->at(track).data(samp_off),
			frames * sizeof(float) );
		track_start += frames;
	    }
	}
    }

    void SCopyPaste4MixerImpl::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	float *s0, *s1, *s2, *s3;
	float g0, g1, g2, g3;
	unsigned tt, tracks = _tracks->size();

	g0 = _gains[0]; s0 = _cache[0];
	g1 = _gains[1]; s1 = _cache[1];
	g2 = _gains[2]; s2 = _cache[2];
	g3 = _gains[3]; s3 = _cache[3];
	// Mix 4 tracks at a time.
	switch(tracks) {
	case 0: s0 = 0; g0 = 0.0f;
	case 1: s1 = 0; g1 = 0.0f;
	case 2: s2 = 0; g2 = 0.0f;
	case 3: s3 = 0; g3 = 0.0f;
	}
	Subroutines::copy_buffers_apply_gain_4( _out, _nframes,
						s0, g0,
						s1, g1,
						s2, g2,
						s3, g3 );
	for( tt=4 ; tt < tracks ;  ) {
	    s0 = _cache[tt]; g0 = _gains[tt++];
	    s1 = _cache[tt]; g1 = _gains[tt++];
	    s2 = _cache[tt]; g2 = _gains[tt++];
	    s3 = _cache[tt]; g3 = _gains[tt++];
	    Subroutines::add_buffers_apply_gain_4( _out, _nframes,
						   s0, g0,
						   s1, g1,
						   s2, g2,
						   s3, g3 );
	}
    }

    void SCopyPaste4MixerImpl::post_process()
    {}

} // namespace MixerTests
