/**
 * BlockedPortStyleMixerImpl.hpp
 *
 * A reference mixing implementation.  Emphasis is on accuracy rather
 * than speed.
 */
#ifndef __MIXERTESTS_BLOCKEDPORTSTYLEMIXERIMPL_HPP_
#define __MIXERTESTS_BLOCKEDPORTSTYLEMIXERIMPL_HPP_

#include "Mixer.hpp"
#include <vector>
#include <deque>

namespace MixerTests
{
    /**
     * Mix buffers by copying chunks to the stack.
     */
    class BlockedPortStyleMixerImpl : public Mixer
    {
    public:
	BlockedPortStyleMixerImpl();
	virtual ~BlockedPortStyleMixerImpl() {}

	/* Non-timed routines */
	virtual std::string name();
	virtual void set_sequencer(Sequencer* seq);
	virtual void set_tracks(std::deque<Track>* tracks);
	virtual void locate(uint32_t frame, uint32_t nframes, float* out);

	/* Routines that will be timed */
	virtual void pre_process();
	virtual void mix();
	virtual void post_process();

    private:
	std::string _name;
	uint32_t _pos;
	uint32_t _nframes;
	float* _out;
	Sequencer* _seq;
	std::deque<Track>* _tracks;
	std::vector<float> _buffers;
	std::vector<float*> _ports;
    };

} // namespace MixerTests

#endif // __MIXERTESTS_BLOCKEDPORTSTYLEMIXERIMPL_HPP_
