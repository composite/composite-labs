/**
 * CopyPasteMixerImpl.cpp
 *
 */

#include "CopyPasteMixerImpl.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>

#include <sys/time.h>
#include <sys/resource.h>

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    CopyPasteMixerImpl::CopyPasteMixerImpl() :
	_name("CopyPasteMixerImpl"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string CopyPasteMixerImpl::name() {
	return _name;
    }

    void CopyPasteMixerImpl::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void CopyPasteMixerImpl::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void CopyPasteMixerImpl::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);

	// Check that we don't smash the stack
	uint32_t n_cache = ((_tracks->size()/8)+1)*8;
	uint32_t cache = n_cache * nframes * sizeof(float);

	struct rlimit rl;
	if( getrlimit(RLIMIT_STACK, &rl) ) {
	    assert(false); // could not get rlimit()
	}
	assert( rl.rlim_cur > cache );

    }

    void CopyPasteMixerImpl::pre_process()
    {}

    void CopyPasteMixerImpl::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int track, n_tracks = _seq->tracks();
	int n_cache = ((n_tracks / 8) + 1) * 8;
	uint32_t start, end, track_start, f, frames, samp_off;
	bool res;
	Sequencer::bits_t playing;
	Sequencer::node_t node;

	float cache[n_cache][_nframes];
	float gains[n_cache];

	memset(cache, 0, n_cache * _nframes * sizeof(float));
	memset(gains, 0, n_cache * sizeof(float));
	memset(_out, 0, _nframes * sizeof(float));

	start = _pos;
	end = _pos + _nframes;

	_seq->locate(start);
	playing = _seq->tracks_playing_in_range(start, end);

	float mixer_gain = 1.0 / static_cast<float>(n_tracks);
	for( track = 0 ; track < n_tracks ; ++track ) {
	    if( ! playing[track] ) continue;

	    track_start = start;
	    while(track_start < end) {
		DLOG( if( track_start != start ) cout << "wraparound..." << endl );
		res = _seq->next(track, track_start, node);
		if( !res ) {
		    track_start = end;
		    continue;
		}
		if( node.start >= end ) {
		    track_start = end;
		    continue;
		}
		// Clip the node
		if( node.end > end ) {
		    node.end = end;
		}
		if( node.start < track_start ) {
		    node.offset += (track_start - node.start);
		    node.start = track_start;
		}
		samp_off = node.offset;
		frames = node.end - node.start;
		assert( frames > 0 );
		assert( frames <= (end - track_start) );
		assert( frames <= (end - start) );
		assert( samp_off < _tracks->at(track).size() );

		gains[track] = node.gain * mixer_gain; // mixer_gain... divide output by number of tracks
		f = track_start - start;
		DLOG( cout << "@" << _pos << " -- "
		      << "copy to cache[" << track << "][" << f << "] "
		      << "from track + " << samp_off
		      << " (" << frames << " frames" << endl );
		memcpy( &cache[track][f],
			_tracks->at(track).data(samp_off),
			frames * sizeof(float) );
		track_start += frames;
	    }
	}
	// Mix 8 tracks at a time.
	int cc = n_cache;
	while(cc) {
	    cc -= 8;
	    frames = _nframes;
	    while(frames) {
		frames--;
		_out[frames] += (
		    cache[cc][frames] * gains[cc] +
		    cache[cc+1][frames] * gains[cc+1] +
		    cache[cc+2][frames] * gains[cc+2] +
		    cache[cc+3][frames] * gains[cc+3] +
		    cache[cc+4][frames] * gains[cc+4] +
		    cache[cc+5][frames] * gains[cc+5] +
		    cache[cc+6][frames] * gains[cc+6] +
		    cache[cc+7][frames] * gains[cc+7]
		    );
	    }
	}
    }

    void CopyPasteMixerImpl::post_process()
    {}

} // namespace MixerTests
