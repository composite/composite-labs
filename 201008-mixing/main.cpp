
#include <stdint.h>
#include <deque>
#include <iostream>

#include "Sequencer.hpp"
#include "TestRunner.hpp"
#include "Track.hpp"
#include "Mixer.hpp"
#include "track_data.hpp"

using namespace std;
using namespace MixerTests;

void load_all_tracks(deque<Track>& tracks)
{
    int N = track_data_get_track_count();
    for(int k = 0; k<N ; ++k) {
	tracks.push_back( Track() );
	tracks[k].load_track(k);
	cout << tracks[k].sum() << endl;
    }
}

int main(void)
{
    TestRunner runner;
    Sequencer seq;

    deque<Track> tracks;
    deque<Track>::iterator track_it;
    deque<Mixer*> mixers;
    deque<Mixer*>::iterator mixer_it;

    load_config("track_data.conf");

    load_all_tracks(tracks);
    load_all_mixers(mixers);
    load_all_sequences(seq);

    runner.set_sequencer(&seq);
    runner.set_tracks(&tracks);
    int mx_size = track_data_get_master_mixdown_size();
    float *mixdown = new float[mx_size];
    track_data_copy_master_mixdown(mixdown, mx_size);
    runner.set_master_mixdown(mixdown, mx_size);
    delete mixdown;

    for(mixer_it = mixers.begin() ; mixer_it != mixers.end() ; ++mixer_it ) {
	runner.set_mixer(*mixer_it);
	runner.run_test(512);
	runner.report();
    }

    return 0;
}
