/**
 * track_data.hpp
 *
 * Methods for accessing the generated data
 */
#ifndef __TRACK_DATA__
#define __TRACK_DATA__

#include <stdint.h>

namespace MixerTests
{
    typedef struct {
	float gain;
	uint32_t offset, start, end;
    } track_data_seq_node_t;

    void load_config(const char*);
    int track_data_get_track_count();
    int track_data_get_track_length(int id);
    int track_data_copy_data(int id, float* dest, int n);
    int track_data_get_master_mixdown_size();
    int track_data_copy_master_mixdown(float* dest, int n);
    int track_data_get_sequence_count(int track_id);
    int track_data_copy_sequence(int id, track_data_seq_node_t* buf, int n);

} // namespace MixerTest

#endif /* __TRACK_DATA__ */
