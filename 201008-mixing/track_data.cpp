/**
 * track_data.cpp
 *
 * Access files with test data
 */

#include <cstring>
#include "track_data.hpp"

#include <iostream>
#include <deque>
#include <fstream>
#include <unistd.h>
#include <cassert>

using namespace std;

namespace MixerTests
{

    typedef deque<string> fn_vec;
    typedef deque<track_data_seq_node_t> tseq_vec;
    typedef deque<tseq_vec> seq_vec;
    typedef deque<float> float_vec;

    typedef struct {
	string conf;

	// Conf file data
	uint32_t oall_length;
	int tracks;
	string sequence_file;
	string mixdown_file;
	fn_vec track_files;

	// Misc data
	deque<uint32_t> lengths;
	seq_vec sequence;
	uint32_t mixdown_length;
    } config_t;

    static config_t g_conf;

    static void load_float_file(string fn, float_vec& dest)
    {
	FILE *inf = fopen(fn.c_str(), "rb");
	int fd = fileno(inf);
	const uint32_t stride = 1024;
	float buf[1024];
	ssize_t bytes;

	dest.clear();
	bytes = read(fd, &buf[0], stride*sizeof(float));
	assert(buf[0] == 0.0f);
	while(bytes) {
	    assert( bytes % 4 == 0 );
	    dest.insert( dest.end(), &buf[0], &buf[bytes/sizeof(float)] );
	    bytes = read(fd, &buf[0], stride*sizeof(float));
	}
	fclose(inf);
    }

    static void load_sequence_file(string fn, seq_vec seq)
    {
	FILE *inf = fopen(fn.c_str(), "rb");
	int fd = fileno(inf);
	uint16_t count;
	track_data_seq_node_t node;
	ssize_t bytes;

	bytes = read(fd, &count, sizeof(count));
	while(bytes) {
	    assert(bytes == 2);
	    tseq_vec ts;
	    while(count--) {
		bytes = read(fd, &node, sizeof(node));
		assert(bytes == 16);
		ts.push_back(node);
	    }
	    g_conf.sequence.push_back(ts);
	    bytes = read(fd, &count, sizeof(count));
	}
	fclose(inf);
    }

    void load_config(const char* fn)
    {
	// Load conf file
	g_conf.conf = string(fn);
	ifstream ifs;

	ifs.open(fn);
	ifs >> g_conf.oall_length;
	ifs >> g_conf.tracks;
	ifs >> g_conf.sequence_file;
	ifs >> g_conf.mixdown_file;
	while( ! ifs.eof() ) {
	    string tmp;
	    ifs >> tmp;
	    if( tmp.length() ) g_conf.track_files.push_back(tmp);
	}
	ifs.close();

	float_vec buf;
	// Enumerate track lengths
	load_float_file(g_conf.mixdown_file, buf);
	g_conf.mixdown_length = buf.size();

	fn_vec::iterator fit;
	for( fit = g_conf.track_files.begin() ; fit != g_conf.track_files.end() ; ++ fit ) {
	    load_float_file( (*fit), buf );
	    g_conf.lengths.push_back( buf.size() );
	}

	// Load the sequence
	load_sequence_file(g_conf.sequence_file, g_conf.sequence);
    }

    int track_data_get_track_count()
    {
        return g_conf.tracks;
    }


    int track_data_get_track_length(int id)
    {
	return (int)g_conf.lengths[id];
    }


    int track_data_copy_data(int id, float* dest, int n)
    {
	float_vec buf;
	load_float_file( g_conf.track_files[id], buf );
	if( n > buf.size() ) n = buf.size();

	uint32_t k;
	for( k = 0 ; k < n ; ++k ) {
	    dest[k] = buf[k];
	}

        return n;
    }

    int track_data_get_master_mixdown_size()
    {
	return g_conf.mixdown_length;
    }

    int track_data_copy_master_mixdown(float* dest, int n)
    {
	float_vec buf;
	load_float_file( g_conf.mixdown_file, buf );
	if( n > buf.size() ) n = buf.size();

	uint32_t k;
	for( k = 0 ; k < n ; ++k ) {
	    dest[k] = buf[k];
	}

        return n;
    }

    int track_data_get_sequence_count(int id)
    {
	return g_conf.sequence[id].size();
    }


    int track_data_copy_sequence(int id, track_data_seq_node_t* dest, int n)
    {
	int k;
	assert(n>0);
	if( unsigned(n) > g_conf.sequence[id].size() )
	    n = g_conf.sequence[id].size();

	for( k = 0 ; k < n ; ++k ) {
	    dest[k] = g_conf.sequence[id][k];
	}

        return k;
    }

} // namespace MixerTests
