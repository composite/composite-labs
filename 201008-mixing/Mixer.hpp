/**
 * Mixer.hpp
 *
 * Generic Mixer interface
 */
#ifndef __MIXERTESTS_MIXER_HPP_
#define __MIXERTESTS_MIXER_HPP_

#include <deque>
#include <string>
#include <stdint.h>

namespace MixerTests
{
    class Sequencer;
    class Track;

    /**
     * Defines a generic mixer for testing various algorithms.
     *
     * It will not be implemented like the current Composite...
     * but will be somewhat flexible regarding whether it
     * holds buffers or tries to mix-from-sources.
     */
    class Mixer
    {
    public:
	virtual ~Mixer() {}

	/* Non-timed routines */
	virtual std::string name() = 0;
	virtual void set_sequencer(Sequencer* seq) = 0;
	virtual void set_tracks(std::deque<Track>* tracks) = 0;
	virtual void locate(uint32_t frame, uint32_t nframes, float* out) = 0;

	/* Routines that will be timed */
	virtual void pre_process() = 0;
	virtual void mix() = 0;
	virtual void post_process() = 0;
    };

    void load_all_mixers(std::deque<Mixer*>& mixers);
    void delete_all_mixers(std::deque<Mixer*>& mixers);

} // namespace MixerTests

#endif // __MIXERTESTS_MIXER_HPP_
