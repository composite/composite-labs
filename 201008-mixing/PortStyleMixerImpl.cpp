/**
 * PortStyleMixerImpl.cpp
 *
 */

#include "PortStyleMixerImpl.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>

#include <sys/time.h>
#include <sys/resource.h>

#include "mixing_subroutines.hpp"

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    PortStyleMixerImpl::PortStyleMixerImpl() :
	_name("PortStyleMixerImpl"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string PortStyleMixerImpl::name() {
	return _name;
    }

    void PortStyleMixerImpl::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void PortStyleMixerImpl::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void PortStyleMixerImpl::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);
	if( _ports.size() != _tracks->size() ) {
	    for(unsigned k=0 ; k<_tracks->size() ; ++k ) {
		_ports.push_back( std::vector<float>() );
	    }
	}
	for(unsigned k=0 ; k<_tracks->size() ; ++k ) {
	    if( _ports[k].size() < nframes ) {
		_ports[k].clear();
		_ports[k].insert(_ports[k].begin(), nframes, 0.0f);
	    }
	}
    }

    void PortStyleMixerImpl::pre_process()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	uint32_t track, n_tracks = _seq->tracks();
	Sequencer::node_t node;
	uint32_t start, end, track_start, f, frames, samp_off;
	uint32_t nframes = _nframes;
	bool res;

	start = _pos;
	end = start + nframes;

	// Copy data to each port.
	for(track=0 ; track<n_tracks ; ++track) {
	    // Zero buffer
	    Subroutines::zero_buffer( &_ports[track][0], nframes );

	    // Copy in bits from the sequence
	    track_start = start;
	    while(track_start < end) {
		DLOG( if( track_start != start ) cout << "wraparound..." << endl );
		res = _seq->next(track, track_start, node);
		if( !res ) {
		    track_start = end;
		    continue;
		}
		if( node.start >= end ) {
		    track_start = end;
		    continue;
		}
		// Clip the node
		if( node.end > end ) {
		    node.end = end;
		}
		if( node.start < track_start ) {
		    node.offset += (track_start - node.start);
		    node.start = track_start;
		}
		samp_off = node.offset;
		frames = node.end - node.start;
		assert( frames > 0 );
		assert( frames <= (end - track_start) );
		assert( frames <= (end - start) );
		assert( samp_off < _tracks->at(track).size() );

		f = track_start - start;
		DLOG( cout << "@" << _pos << " -- "
		      << "copy to cache[" << track << "][" << f << "] "
		      << "from track + " << samp_off
		      << " (" << frames << " frames" << endl );
		Subroutines::copy_buffer_apply_gain(
		    &_ports[track][f],
		    _tracks->at(track).data(samp_off),
		    frames,
		    node.gain
		    );
		track_start += frames;
	    }
	}

    }

    void PortStyleMixerImpl::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int track, n_tracks = _seq->tracks();
	uint32_t nframes = _nframes;
	float mixer_gain = 1.0/float(n_tracks);

	if(!n_tracks) {
	    Subroutines::zero_buffer(_out, nframes);
	    return;
	}
	Subroutines::copy_buffer_apply_gain( _out, &_ports[0][0], nframes, mixer_gain );
	for( track = 1 ; track < n_tracks ; ++track ) {
	    Subroutines::add_buffer_apply_gain( _out, &_ports[track][0], nframes, mixer_gain );
	}
    }

    void PortStyleMixerImpl::post_process()
    {}

} // namespace MixerTests
