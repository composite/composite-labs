/**
 * TestRunner.cpp
 *
 */

#include <time.h>
#include <iostream>
#include <cassert>
#include <cmath>
#include "TestRunner.hpp"
#include "Track.hpp"
#include "Mixer.hpp"
#include "Sequencer.hpp"

using namespace std;

namespace MixerTests
{
    TestRunner::TestRunner() :
	_seq(0),
	_mix(0)
    {}

    TestRunner::~TestRunner()
    {}

    void TestRunner::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void TestRunner::set_mixer(Mixer* mix)
    {
	_mix = mix;
    }

    void TestRunner::set_tracks(tracks_t* tracks)
    {
	_tracks = tracks;
    }

    void TestRunner::set_master_mixdown(float *mix, uint32_t size)
    {
	_master_mixdown.clear();
	_master_mixdown.insert( _master_mixdown.end(), mix, mix+size );
    }

    void TestRunner::run_test(uint32_t nframes)
    {
	uint32_t pos, end, k;
	clock_t start, c_pre, c_mix, c_post;
	vector<float> _out(nframes+64, 0.0f);
	float *out = &_out[0];

	if( (unsigned long)out % 16 != 0 ) ++out;
	if( (unsigned long)out % 16 != 0 ) ++out;
	if( (unsigned long)out % 16 != 0 ) ++out;
	if( (unsigned long)out % 16 != 0 ) ++out;
	assert( (unsigned long)out % 16 == 0 );

	_mix->set_sequencer(_seq);
	_mix->set_tracks(_tracks);

	_pre_clocks = 0;
	_mix_clocks = 0;
	_post_clocks = 0;
	_out_sum = 0;

	end = _seq->end();
	for( pos = 0 ; pos < end ; pos += nframes ) {
	    _mix->locate(pos, nframes, out);
	    start = clock();
	    _mix->pre_process();
	    c_pre = clock();
	    _mix->mix();
	    c_mix = clock();
	    _mix->post_process();
	    c_post = clock();

	    _pre_clocks += (c_pre - start);
	    _mix_clocks += (c_mix - c_pre);
	    _post_clocks += (c_post - c_mix);

	    for( k = 0 ; k < nframes ; ++k ) {
		_out_sum += out[k];
		if( (pos+k) < _master_mixdown.size() ) {
		    assert( fabs(out[k] - _master_mixdown[pos+k]) < 1e-5 );
		} else {
		    assert( fabs(out[k]) < 1e-5 );
		}
	    }	    
	}

	// delete [] _out;
    }

    void TestRunner::report()
    {
	uint32_t total = _pre_clocks + _mix_clocks + _post_clocks;

	cout << _mix->name() << ": "
	     << _pre_clocks << " "
	     << _mix_clocks << " "
	     << _post_clocks << " "
	     << total << " "
	     << _out_sum << endl;
    }

} // namespace MixerTests
