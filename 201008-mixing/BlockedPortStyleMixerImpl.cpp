/**
 * BlockedPortStyleMixerImpl.cpp
 *
 */

#include "BlockedPortStyleMixerImpl.hpp"
#include "Sequencer.hpp"
#include "Track.hpp"
#include <cstring>
#include <cassert>

#include <sys/time.h>
#include <sys/resource.h>

#include "mixing_subroutines.hpp"

// #define MIX_DEBUG

#if defined MIX_DEBUG
#include <iostream>
using namespace std;
#define DLOG(x) x
#else // MIX_DEBUG
#define DLOG(x) void(0)
#endif // MIX_DEBUG

namespace MixerTests
{
    BlockedPortStyleMixerImpl::BlockedPortStyleMixerImpl() :
	_name("BlockedPortStyleMixerImpl"),
	_pos(0),
	_nframes(0),
	_out(0),
	_seq(0),
	_tracks(0)
    {}

    std::string BlockedPortStyleMixerImpl::name() {
	return _name;
    }

    void BlockedPortStyleMixerImpl::set_sequencer(Sequencer* seq)
    {
	_seq = seq;
    }

    void BlockedPortStyleMixerImpl::set_tracks(std::deque<Track>* tracks)
    {
	_tracks = tracks;
    }

    void BlockedPortStyleMixerImpl::locate(uint32_t frame, uint32_t nframes, float* out)
    {
	_pos = frame;
	_nframes = nframes;
	_out = out;
	_seq->locate(frame);
	const uint32_t opt_stride = 64 / sizeof(float);
	uint32_t min_buf = _tracks->size() * nframes;
	min_buf = ((min_buf/opt_stride)+1) * opt_stride;

	if( _buffers.size() < min_buf ) {
	    _buffers.clear();
	    _buffers.insert( _buffers.end(), min_buf, 0.0f );
	}
	if( _ports.size() < _tracks->size() ) {
	    _ports.insert( _ports.end(), _tracks->size() - _ports.size() , 0 );
	}
	for( unsigned k = 0 ; k < _ports.size() ; ++k ) {
	    _ports[k] = &_buffers[k*nframes];
	}
    }

    void BlockedPortStyleMixerImpl::pre_process()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	uint32_t track, n_tracks = _seq->tracks();
	Sequencer::node_t node;
	uint32_t start, end, track_start, f, frames, samp_off;
	uint32_t nframes = _nframes;
	bool res;

	start = _pos;
	end = start + nframes;

	// Subroutines::zero_buffer( &_buffers[0], n_tracks*nframes );

	// Copy data to each port.
	for(track=0 ; track<n_tracks ; ++track) {
	    // Zero buffer
	    //Subroutines::zero_buffer( &_ports[track][0], nframes );

	    // Copy in bits from the sequence
	    track_start = start;
	    while(track_start < end) {
		DLOG( if( track_start != start ) cout << "wraparound..." << endl );
		res = _seq->next(track, track_start, node);
		if( !res ) {
		    Subroutines::zero_buffer( &_ports[track][track_start-start], end-track_start );
		    track_start = end;
		    continue;
		}
		if( node.start >= end ) {
		    Subroutines::zero_buffer( &_ports[track][track_start-start], end-track_start );
		    track_start = end;
		    continue;
		}
		// Clip the node
		if( node.end > end ) {
		    node.end = end;
		}
		if( node.start < track_start ) {
		    node.offset += (track_start - node.start);
		    node.start = track_start;
		}
		if( node.start > track_start ) {
		    Subroutines::zero_buffer( &_ports[track][track_start-start],
					      track_start - node.start );
		}
		samp_off = node.offset;
		frames = node.end - node.start;
		assert( frames > 0 );
		assert( frames <= (end - track_start) );
		assert( frames <= (end - start) );
		assert( samp_off < _tracks->at(track).size() );

		f = track_start - start;
		DLOG( cout << "@" << _pos << " -- "
		      << "copy to cache[" << track << "][" << f << "] "
		      << "from track + " << samp_off
		      << " (" << frames << " frames" << endl );
		Subroutines::copy_buffer_apply_gain(
		    &_ports[track][f],
		    _tracks->at(track).data(samp_off),
		    frames,
		    node.gain
		    );
		track_start += frames;
	    }
	}

    }

    void BlockedPortStyleMixerImpl::mix()
    {
	assert(_tracks);
	assert(_seq);
	assert(_out);

	int track, n_tracks = _seq->tracks();
	uint32_t nframes = _nframes;
	float mixer_gain = 1.0/float(n_tracks);

	uint32_t total = nframes * n_tracks;
	uint32_t mask = nframes - 1;
	Subroutines::zero_buffer(_out, nframes);

	float *o, *i;
	uint32_t outer, inner, save_inner;
	o = &_out[0];
	i = &_buffers[0];
	outer = n_tracks;
	save_inner = inner = nframes/8;
	while(outer--) {
	    while(inner--) {
		(*o++) += (*i++);
		(*o++) += (*i++);
		(*o++) += (*i++);
		(*o++) += (*i++);
		
		(*o++) += (*i++);
		(*o++) += (*i++);
		(*o++) += (*i++);
		(*o++) += (*i++);
	    }
	    o = &_out[0];
	    inner = save_inner;
	}
	inner = save_inner;
	o = &_out[0];
	while(inner--) {
	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;

	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;
	    (*o++) *= mixer_gain;
	}
    }

    void BlockedPortStyleMixerImpl::post_process()
    {}

} // namespace MixerTests
