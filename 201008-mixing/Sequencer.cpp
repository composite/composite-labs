/**
 * Sequencer.cpp
 */

#include "Sequencer.hpp"
#include "track_data.hpp"

namespace MixerTests
{
    Sequencer::Sequencer()
    {}

    Sequencer::~Sequencer()
    {}

    void Sequencer::load_all_sequences()
    {
	int tracks, seqs;
	track_t t_s;
	node_t n = { 0.0f, 0, 0, 0 };
	int k;

	tracks = track_data_get_track_count();
	for( k = 0 ; k < tracks ; ++k ) {
	    t_s.clear();
	    seqs = track_data_get_sequence_count(k);
	    t_s.assign(seqs, n);
	    track_data_copy_sequence(k, &t_s[0], seqs);
	    _seq.push_back(t_s);
	}

	// Find the end;
	uint32_t tmp, end = 0;
	for( k = 0 ; k < tracks ; ++k ) {
	    tmp = _seq[k].back().end;
	    if( tmp > end ) end = tmp;
	}
	_end = end + 1;

	_heads.clear();
	_heads.assign( _seq.size(), 0 );
	_pos = 0;

	locate(0); // just in case...
    }

    void Sequencer::locate(uint32_t pos)
    {
	if(pos == _pos) return;
	if(pos < _pos) {
	    _pos = 0;
	    _heads.clear();
	    _heads.assign( _seq.size(), 0 );
	}

	int j;
	uint8_t ptr;
	for( j = 0 ; j < tracks() ; ++j ) {
	    ptr = _heads[j];
	    while( (ptr < _seq[j].size()) && (pos < _seq[j][ptr].end) )
		++ptr;
	    if(ptr < _seq[j].size()) _heads[j] = ptr;
	}

	_pos = pos;
    }

    Sequencer::bits_t Sequencer::tracks_playing_in_range(uint32_t start, uint32_t end)
    {
	bits_t playing( tracks(), false );
	int k, N;
	node_t node;
	bool rv;

	N = tracks();
	for( k=0 ; k<N ; ++k ) {
	    rv = next(k, start, node);
	    if(node.start < end) {
		playing[k] = true;
	    }
	}

	return playing;
    }

    bool Sequencer::next(int track, uint32_t frame, node_t& node)
    {
	uint8_t ptr = _heads[track];
	while( (ptr < _seq[track].size()) && (_seq[track][ptr].end <= frame) )
	    ++ptr;
	if( ptr < _seq[track].size() ) {
	    node = _seq[track][ptr];
	    return true;
	}
	return false;
    }

} // namespace MixerTests
