/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Delegate.hpp"
#include "Engine.hpp"

#include <QtCore/QDebug>

#include <QtGui/QGraphicsObject>

#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeContext>

Delegate::Delegate()
{
}

Delegate::~Delegate()
{
}

void Delegate::set_view(QDeclarativeView *view)
{
    m_view = view;
}

void Delegate::set_engine(Engine *e)
{
    m_engine = e;
}

void Delegate::init()
{
    QObject *v = m_view->rootObject();
    QObject *e = m_engine;

    connect(v, SIGNAL(data(QString)),
            e, SLOT(keypress(QString)));
    connect(e, SIGNAL(content_changed(QString)),
            v, SLOT(contentChanged(QString)));
    connect(v, SIGNAL(quit()),
            m_view, SLOT(close()));
}
