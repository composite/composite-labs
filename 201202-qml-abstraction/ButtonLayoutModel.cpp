/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "ButtonLayoutModel.hpp"
#include <cassert>
#include <iostream>
#include "Engine.hpp"

using namespace std;

ButtonLayoutModel::ButtonLayoutModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

ButtonLayoutModel::~ButtonLayoutModel()
{
}

int ButtonLayoutModel::columnCount(const QModelIndex& /*parent*/) const
{
    return m_engine->columns();
}

int ButtonLayoutModel::rowCount(const QModelIndex& /*parent*/) const
{
    return m_engine->rows();
}

void ButtonLayoutModel::buttonPressed(int button_id)
{
    m_engine->event(button_id, Engine::E_BUTTON_CLICK);
}

QVariant ButtonLayoutModel::data(const QModelIndex& index, int role) const
{
    int button_id;

    button_id = m_engine->button_id(index.row(), index.column());
    switch (role) {
    case Qt::DisplayRole:
        return m_engine->button_text(button_id);
        break;
    case Qt::UserRole:
        return QVariant(button_id);
        break;
    }
    return QVariant();
}
