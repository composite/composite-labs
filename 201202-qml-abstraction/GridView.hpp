/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __GRID_VIEW_HPP__
#define __GRID_VIEW_HPP__

#include <QDeclarativeItem>

class QDeclarativeComponent;
class QAbstractItemModel;
class QModelIndex;

class GridView : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(QVariant model READ model WRITE setModel NOTIFY modelChanged);
    Q_PROPERTY(QDeclarativeComponent* delegate READ delegate WRITE setDelegate NOTIFY delegateChanged);
    Q_PROPERTY(float margin READ margin WRITE setMargin);

public:
    GridView(QDeclarativeItem* parent = 0);
    ~GridView();

    QVariant model() const;
    void setModel(const QVariant& mod);

    QDeclarativeComponent* delegate() const;
    void setDelegate(QDeclarativeComponent* del);

    float margin() const;
    void setMargin(float m);

signals:
    void modelChanged();
    void delegateChanged();
    void cellWidthChanged();
    void cellHeightChanged();

private:
    QAbstractItemModel *m_model;
    QDeclarativeComponent *m_delegate;
    float m_margin;

protected slots:
    /* Slots to be handled QAbstractItemModel */
    void columnsInserted(const QModelIndex& parent, int start, int end);
    void columnsMoved(const QModelIndex& sourceParent, int sourceStart, int sourceEnd,
                      const QModelIndex& destinationParent, int destinationColumn);
    void columnsRemoved(const QModelIndex& parent, int start, int end);
    void dataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight);
    void headerDataChanged(Qt::Orientation orientation, int first, int last);
    void layoutChanged();
    void modelReset();
    void rowsInserted(const QModelIndex& parent, int start, int end);
    void rowsMoved(const QModelIndex& sourceParent, int sourceStart, int sourceEnd,
                   const QModelIndex& destinationParent, int destinationRow);
    void rowsRemoved(const QModelIndex& parent, int start, int end);

protected:
    virtual void geometryChanged(const QRectF& newGeometry, const QRectF& oldGeometry);

private:
    void update_layout();

}; // class GridView

#endif // __GRID_VIEW_HPP__
