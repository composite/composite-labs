/*
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __ENGINE_HPP__
#define __ENGINE_HPP__

#include <QtCore/QObject>
#include <QtCore/QString>

class Engine : public QObject
{
    Q_OBJECT
public:
    Engine();
    ~Engine();

    typedef enum {
        M_CLEAR,
        M_ADD,
        M_SUB,
        M_MUL,
        M_DIV,
    } mode_t;

    typedef enum {
        E_NONE = 0,
        E_BUTTON_CLICK = 1,
    } event_t;

    QString get_display();

    int columns() {
        return 4;
    }
    int rows() {
        return 4;
    }
    /* zero-offset for row and col */
    int button_id(int row, int col) {
        return row*columns() + col;
    }
    QString button_text(int button_id);

public slots:
    void keypress(QString val);
    void event(int button_id, int event);

signals:
    void content_changed(QString val);

private:
    void p_error();
    void p_input_digit(QString val);
    void p_input_oper(QString val);
    void p_calculate();
    void p_clear();

private:
    mode_t m_mode;
    QString m_display;
    double m_acc;
    bool m_new_mode;

}; // class Delegate

#endif // __ENGINE_HPP__
