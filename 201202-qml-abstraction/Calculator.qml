/*                                      -*- mode:javascript -*-
 * calculator.qml
 *
 * This code was written by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 * in 2011-2012.  It is placed in the public domain.
 */

import QtQuick 1.0
import "." as Local
import Foo 1.0 as Local

Rectangle {
    id: iRoot;
    color: "gray";
    width: 200; height: 252;
    anchors.margins: 5;
    property string text: "0";
    focus: true; /* for keyboard input */

    signal data(string val);
    onData: {
	console.log("iRoot: " + val)
    }

    signal contentChanged(string val);
    onContentChanged: {
	text = val;
    }

    signal clear;
    onClear: {
	text = "0";
    }

    signal quit;

    Rectangle {
	id: iDisplay;
	color: "white";
	anchors.top: parent.top;
	anchors.left: parent.left;
	anchors.right: parent.right;
	anchors.margins: parent.anchors.margins;
	height: 42;
	radius: height/10;

	Text {
	    id: iDisplayText;
	    text: iRoot.text;
	    color: "black";
	    font.pixelSize: 24;
	    anchors.verticalCenter: parent.verticalCenter;
	    anchors.right: parent.right;
	    anchors.margins: 10;

	}
    }

    Component {
	id: iButtonDelegate;

	Button {
	    
	}
    }

    Local.GridView {
	id: button_grid;
	property color button_color_top: "white";
	property color button_color_bot: "gray";
	property color text_color: "black";

	anchors.top: iDisplay.bottom;
	anchors.left: iRoot.left;
	anchors.right: iRoot.right;
	anchors.bottom: iRoot.bottom;
	anchors.margins: parent.anchors.margins;

	model: button_model;
	delegate: iButtonDelegate;
	margin: 7;

    }

    Keys.onPressed: {
	switch (event.key) {
	case Qt.Key_0:
            iRoot.data("0");
            break;
	case Qt.Key_1:
            iRoot.data("1");
            break;
	case Qt.Key_2:
            iRoot.data("2");
            break;
	case Qt.Key_3:
            iRoot.data("3");
            break;
	case Qt.Key_4:
            iRoot.data("4");
            break;
	case Qt.Key_5:
            iRoot.data("5");
            break;
	case Qt.Key_6:
            iRoot.data("6");
            break;
	case Qt.Key_7:
            iRoot.data("7");
            break;
	case Qt.Key_8:
            iRoot.data("8");
            break;
	case Qt.Key_9:
            iRoot.data("9");
            break;
	case Qt.Key_C:
            iRoot.data("C");
            break;
	case Qt.Key_Plus:
            iRoot.data("+");
            break;
	case Qt.Key_Minus:
            iRoot.data("-");
            break;
	case Qt.Key_Asterisk:
            iRoot.data("*");
            break;
	case Qt.Key_Slash:
            iRoot.data("/");
            break;
	case Qt.Key_Equal:
	case Qt.Key_Enter:
	case Qt.Key_Return:
            iRoot.data("=");
            break;
	case Qt.Key_Escape:
            iRoot.quit();
            break;
	}
    }
}
