/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "GridView.hpp"

#include <iostream>
#include <cassert>

#include <QtCore/QAbstractItemModel>
#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtDeclarative/QDeclarativeComponent>
#include <QtDeclarative/QDeclarativeProperty>

GridView::GridView(QDeclarativeItem *parent) :
    QDeclarativeItem(parent),
    m_model(0),
    m_delegate(0),
    m_margin(5)
{

}

GridView::~GridView()
{
}

QVariant GridView::model() const
{
    return *reinterpret_cast<QVariant*>(m_model);
}

#define M_CONNECT(sender, sig) connect((sender), SIGNAL(sig), this, SLOT(sig))
#define M_DISCONNECT(sender, sig) connect((sender), SIGNAL(sig), this, SLOT(sig))

void GridView::setModel(const QVariant& model)
{
    QObject *obj;
    QAbstractItemModel *mm;
    std::cout << __func__ << std::endl;

    obj = qvariant_cast<QObject*>(model);
    mm = qobject_cast<QAbstractItemModel*>(obj);

    if (!mm) {
        std::cerr << "Can not set model... is not a QAbstractItemModel" << std::endl;
        return;
    }

    if (m_model) {

        M_DISCONNECT(m_model,
                     columnsInserted(const QModelIndex&, int, int));
        M_DISCONNECT(m_model,
                     columnsMoved(const QModelIndex&, int, int, const QModelIndex&, int));
        M_DISCONNECT(m_model,
                     columnsRemoved(const QModelIndex&, int, int));
        M_DISCONNECT(m_model,
                     dataChanged(const QModelIndex&, const QModelIndex&));
        M_DISCONNECT(m_model,
                     headerDataChanged(Qt::Orientation, int, int));
        M_DISCONNECT(m_model,
                     layoutChanged());
        M_DISCONNECT(m_model,
                     modelReset());
        M_DISCONNECT(m_model,
                     rowsInserted(const QModelIndex&, int, int));
        M_DISCONNECT(m_model,
                     rowsMoved(const QModelIndex&, int, int, const QModelIndex&, int));
        M_DISCONNECT(m_model,
                     rowsRemoved(const QModelIndex&, int, int));
    }

    m_model = mm;

    M_CONNECT(m_model,
              columnsInserted(const QModelIndex&, int, int));
    M_CONNECT(m_model,
              columnsMoved(const QModelIndex&, int, int, const QModelIndex&, int));
    M_CONNECT(m_model,
              columnsRemoved(const QModelIndex&, int, int));
    M_CONNECT(m_model,
              dataChanged(const QModelIndex&, const QModelIndex&));
    M_CONNECT(m_model,
              headerDataChanged(Qt::Orientation, int, int));
    M_CONNECT(m_model,
              layoutChanged());
    M_CONNECT(m_model,
              modelReset());
    M_CONNECT(m_model,
              rowsInserted(const QModelIndex&, int, int));
    M_CONNECT(m_model,
              rowsMoved(const QModelIndex&, int, int, const QModelIndex&, int));
    M_CONNECT(m_model,
              rowsRemoved(const QModelIndex&, int, int));

    update_layout();
    emit modelChanged();
}

QDeclarativeComponent* GridView::delegate() const
{
    return m_delegate;
}

void GridView::setDelegate(QDeclarativeComponent* del)
{
    std::cout << __func__ << std::endl;
    m_delegate = del;
    update_layout();
    emit delegateChanged();
}

float GridView::margin() const
{
    return m_margin;
}

void GridView::setMargin(float m)
{
    if (m >= 0.0) {
        m_margin = m;
        update_layout();
    }
}

void GridView::columnsInserted(const QModelIndex& /*parent*/,
                               int /*start*/,
                               int /*end*/)
{
    update_layout();
}

void GridView::columnsMoved(const QModelIndex& /*sourceParent*/,
                            int /*sourceStart*/,
                            int /*sourceEnd*/,
                            const QModelIndex& /*destinationParent*/,
                            int /*destinationColumn*/)
{
    update_layout();
}

void GridView::columnsRemoved(const QModelIndex& /*parent*/,
                              int /*start*/,
                              int /*end*/)
{
    update_layout();
}

void GridView::dataChanged(const QModelIndex& /*topLeft*/,
                           const QModelIndex& /*bottomRight*/)
{
    update_layout();
}

void GridView::headerDataChanged(Qt::Orientation /*orientation*/,
                                 int /*first*/,
                                 int /*last*/)
{
    update_layout();
}

void GridView::layoutChanged()
{
    update_layout();
}

void GridView::modelReset()
{
    update_layout();
}

void GridView::rowsInserted(const QModelIndex& /*parent*/,
                            int /*start*/,
                            int /*end*/)
{
    update_layout();
}

void GridView::rowsMoved(const QModelIndex& /*sourceParent*/,
                         int /*sourceStart*/,
                         int /*sourceEnd*/,
                         const QModelIndex& /*destinationParent*/,
                         int /*destinationRow*/)
{
    update_layout();
}

void GridView::rowsRemoved(const QModelIndex& /*parent*/,
                           int /*start*/,
                           int /*end*/)
{
    update_layout();
}

void GridView::geometryChanged(const QRectF& newGeometry, const QRectF& oldGeometry)
{
    qDebug() << oldGeometry << " ==> " << newGeometry;
    setImplicitWidth(newGeometry.width());
    setImplicitHeight(newGeometry.height());
    update_layout();
}

void GridView::update_layout()
{
    QObjectList kids = children();
    int rows, cols, r, c;
    float x, y, margin;
    float cell_height, cell_width;
    QObject *obj;
    QModelIndex ix;

    if (!m_model || !m_delegate)
        return;

    /* clear out existing children */
    foreach(obj, kids) {
        obj->setParent(0);
        delete obj;
    }

    rows = m_model->rowCount();
    cols = m_model->columnCount();
    cell_height = implicitHeight() / rows;
    cell_width = implicitWidth() / cols;
    margin = GridView::margin();

    y = margin/2;
    for (r = 0 ; r < rows ; ++r) {
        x = margin/2;
        for (c = 0 ; c < cols ; ++c) {
            obj = m_delegate->create();
            obj->setParent(this);
            obj->setProperty("parent", QVariant::fromValue<QDeclarativeItem*>(this));
            obj->setProperty("x", x);
            obj->setProperty("y", y);
            obj->setProperty("width", cell_width - margin);
            obj->setProperty("height", cell_height - margin);
            ix = m_model->index(r, c);
            obj->setProperty("text", m_model->data(ix, Qt::DisplayRole));
            obj->setProperty("button_id", m_model->data(ix, Qt::UserRole));
            connect(obj, SIGNAL(buttonPressed(int)),
                    m_model, SLOT(buttonPressed(int)));
            x += cell_width;
        }
        y += cell_height;
    }

}
