/*
 * Copyright(c) 2012 by Gabriel M. Beddingfield <gabriel@teuton.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __BUTTON_LAYOUT_MODEL_HPP__
#define __BUTTON_LAYOUT_MODEL_HPP__

#include <QAbstractTableModel>

class QObject;
class Engine;

class ButtonLayoutModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ButtonLayoutModel(QObject *parent = 0);
    ~ButtonLayoutModel();

public:
    /* Reimplemented virtual methods */
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    void setEngine(Engine *eng) {
        m_engine = eng;
    }

public slots:
    void buttonPressed(int id);

private:
    Engine *m_engine;

}; // class ButtonLayoutModel

#endif // __BUTTON_LAYOUT_MODEL_HPP__
